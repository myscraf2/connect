<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Route::middleware(['auth:sanctum', 'verified', 'license'])->group(function () {    
    Route::get('/home', function () {
        return Inertia\Inertia::render('Home');
    })->name('home');
    Route::get('products', 'ProductController@index')->name('products');
    //Courses
    Route::resource('webinars', 'WebinarController');
    Route::resource('courses', 'CourseController');
    //Resources
    Route::resource('papers', 'PaperController');
    Route::get('/scales/pdf/{id}','ScaleController@pdf');
    Route::get('/scales/send/{id}','ScaleController@email');
    Route::resource('scales', 'ScaleController');
    Route::post('scales/send', 'ScaleController@send');
    //News
    Route::resource('news', 'NewController');
    //Contact
    Route::get('/contact', function () {
        return Inertia\Inertia::render('Contact');
    })->name('contact');
    //Users
    Route::put('users/activate/{id}', 'UserController@activate');
    Route::resource('users','UserController');
});

Route::get('/privacy', function () {
    return Inertia\Inertia::render('Privacy');
})->name('privacy');

Route::get('/terms', function () {
    return Inertia\Inertia::render('Terms');
})->name('terms');

Route::get('license/verify', function () {
    return Inertia\Inertia::render('License');
})->name('license.notice');

Route::get('/ios', function () {
    return Inertia\Inertia::render('Apps/Ios');
});

Route::get('/android', function () {
    return Inertia\Inertia::render('Apps/Android');
});


/*Route::get('mail', function () {
    //$item = App\Models\Acquisition::find(1);

    //auth()->user()->notify(new \App\Notifications\ScaleResult);

    return (new App\Notifications\ScaleResult)
                ->toMail(null);
});*/

