@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
{{ config('app.name') }}
@endcomponent
@endslot

{{-- Body --}}
{{ $slot }}

{{-- Subcopy --}}
@isset($subcopy)
@slot('subcopy')
@component('mail::subcopy')
{{ $subcopy }}
@endcomponent
@endslot
@endisset

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
AVISO IMPORTANTE: El presente correo electrónico y cualquier archivo que se transmita con el mismo son confidenciales, privilegiados y/o están exentos de revelación, y está destinados exclusivamente para uso del
destinatario designado. Si usted no es el destinatario designado, no debe diseminar, distribuir o copiar este correo
electrónico o los documentos adjuntos. Si ha recibido este correo electrónico por equivocación, sírvase notificar al remitente de inmediato por este medio y luego elimínelo de su sistema. La divulgación, copia o distribución no autorizadas, o la toma de medidas en función del contenido de este correo electrónico y sus adjuntos, quedan estrictamente prohibidas.<br>
© {{ date('Y') }} Apopharma. @lang('All rights reserved.')
@endcomponent
@endslot
@endcomponent
