<div class="min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0 bg-gradient-to-b bg-gradient-to-b from-white to-gray-300">
    <div>
        {{ $logo }}
    </div>

    <div class="w-11/12 sm:max-w-md mt-6 bg-white shadow-md overflow-hidden rounded-t-3xl">
        <div class="flex mb-4">
            <a href="/login" class="w-1/2 bg-blue-app-200 h-15 text-center pt-5 text-white font-bold">
                INICIAR SESIÓN
            </a>
            <a href="/register" class="w-1/2 bg-blue-app-400 h-15 text-center pt-5 text-white font-bold">
                REGISTRO
            </a>
        </div>
    </div>
    <div class="w-11/12 sm:max-w-md px-6 py-4 bg-white shadow-md overflow-hidden rounded-b-3xl mb-10">
        {{ $slot }}
    </div>
</div>
