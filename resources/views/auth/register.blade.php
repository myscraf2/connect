<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>

        <x-jet-validation-errors class="mb-4" />

        <form method="POST" action="{{ route('register') }}">
            @csrf

            <div>
                <x-jet-label for="name" value="{{ __('Full Name') }}" />
                <x-jet-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus autocomplete="name" />
            </div>

            <div class="mt-4">
                <x-jet-label for="professional_license" value="{{ __('Professional License') }}" />
                <x-jet-input id="professional_license" class="block mt-1 w-full" type="text" name="professional_license" :value="old('professional_license')" required autofocus autocomplete="professional_license" />
            </div>

            <div class="mt-4">
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required />
            </div>

            <div class="mt-4">
                <x-jet-label for="password" value="{{ __('Password') }}" />
                <x-jet-input id="password" class="block mt-1 w-full" type="password" name="password" required autocomplete="new-password" />
            </div>

            <div class="mt-4">
                <x-jet-label for="password_confirmation" value="{{ __('Confirm Password') }}" />
                <x-jet-input id="password_confirmation" class="block mt-1 w-full" type="password" name="password_confirmation" required autocomplete="new-password" />
            </div>

            <div class="mt-4">
                <label class="flex items-center">
                    <input type="checkbox" class="form-checkbox" name="terms">
                    <span class="ml-2">Acepto los <a href="/terms" class="underline">términos y condiciones</a> y el <a href="/privacy" class="underline">aviso de privacidad</a></span>
                </label>
            </div>
            <div class="flex items-center justify-center mt-4">
                <x-jet-button class="w-full">
                    {{ __('Register') }}
                </x-jet-button>
            </div>
            <div class="flex items-center justify-center mt-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    {{ __('Already registered?') }}
                </a>
            </div>            
        </form>
    </x-jet-authentication-card>
</x-guest-layout>
