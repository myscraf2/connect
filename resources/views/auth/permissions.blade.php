<script>
    @auth
        window.Laravel = {!! json_encode([ 
            'permissions' => Auth::user()->getCanPermissions()], true) 
        !!};
    @else
        window.Laravel = null;
    @endauth
</script>