@component('mail::message')
# ¡Hola!

Gracias por utilizar Aponet, a continuación encontrará los resultados de la escala realizada:

@component('mail::panel')
Escala: <b>{{ $result->scale->title }}</b><br>
Nombre del paciente: <b>{{ $result->name }}</b><br>
Fecha de realización: <b>{{ \Carbon\Carbon::parse($result->date)->format('m/d/Y')}}</b>
@endcomponent

Háganos llegar sus dudas, comentarios y sugerencias a través de Aponet, en la sección contacto.

Saludos cordiales,<br>
Equipo Aponet
@endcomponent
