<!doctype html>
<html lang="es">
<head>
<meta charset="UTF-8">
<title>Escalas</title>
<style type="text/css">
    * {
        font-family: Verdana, Arial, sans-serif;
    }
    table{
        font-size: x-small;
    }
    tfoot tr td{
        font-weight: bold;
        font-size: x-small;
    }
    .gray {
        background-color: lightgray
    }
    @page {
        margin: 50px 25px 50px 25px;
    }
    footer {
        position: fixed; 
        bottom: 0px; 
        left: 0px; 
        right: 0px;
        height: 20px;
    }
</style>

</head>
<body>

  <table width="100%">
    <tr>
        <td align="center">
            <h1>Escala de {{ ucfirst(trans(\App\Enums\ScaleType::getDescription($result->scale->type))) }} {{ $result->scale->title }}</h1>
        </td>
    </tr>
  </table>

  <table width="100%">
    <tr>
        <td><strong>Paciente:</strong> {{ $result->name }}</td>
    </tr>
    <tr>
        <td><strong>Edad:</strong> {{ $result->age }}</td>
    </tr>
    <tr>
        <td><strong>Fecha:</strong> {{ \Carbon\Carbon::parse($result->date)->format('m/d/Y') }}</td>
    </tr>
  </table>

  <br/>

  <table width="100%">
    <thead style="background-color: lightgray;">
      <tr>
        <th></th>
        <th>Respuesta</th>
        <th>Valor</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($result->details as $detail)      
        <tr>
            <td>{!! $detail->question->title !!}</td>
            <td align="left">{{ $detail->answer->name }}</td>
            <td align="center">{{ $detail->value }}</td>
        </tr>
        @endforeach
    </tbody>

    <tfoot>
        <tr>
            <td></td>
            <td align="right">Puntaje Total: </td>
            <td align="center" class="gray">{{ $result->getScore() }}</td>
        </tr>
    </tfoot>
  </table>

  <table width="100%" style="margin-top:20px;">
    <tr>
        <td align="center">
            <div style="font-size: 14pt;">Diagnóstico<br>{!! $result->getDiagnosis() !!}</div>
        </td>
    </tr>
  </table>

  <div style="margin-top: 20px;text-align:center;font-size:10pt;">
    {!! $result->scale->notes !!}
</div>

<footer>
    <div style="text-align:center;font-size:8pt;">
        {!! $result->scale->copyright !!}<br>
        Los resultados de esta escala no representan un instrumento diagnóstico específico, la interpretación final dependerá del médico tratante.        
    </div>
</footer>

</body>
</html>