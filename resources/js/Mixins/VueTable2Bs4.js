// Bootstrap4
export default {
  table: {
      tableWrapper: '',
      tableHeaderClass: 'mb-0',
      tableBodyClass: 'mb-0',
      tableClass: 'w-full table table-bordered',
      loadingClass: 'loading',
      ascendingIcon: 'fa fa-chevron-up',
      descendingIcon: 'fa fa-chevron-down',
      ascendingClass: 'sorted-asc',
      descendingClass: 'sorted-desc',
      sortableIcon: 'fa fa-sort',
      detailRowClass: 'vuetable-detail-row',
      handleIcon: 'fa fa-bars text-secondary',
      renderIcon(classes, options) {
          return `<i class="${classes.join(' ')}"></span>`
      }
  },
  pagination: {
      wrapperClass: 'float-right text-blue-700 -mt-1',
      activeClass: 'bg-blue-500 hover:bg-blue-700 text-white rounded',
      disabledClass: 'disabled',
      pageClass: 'px-3 py-1 cursor-pointer',
      linkClass: 'text-blue-500 px-3 py-1',
      paginationClass: 'pagination',
      paginationInfoClass: 'float-left',
      dropdownClass: 'form-control',
      icons: {
          first: 'fa fa-angle-double-left',
          prev: 'fa fa-angle-left',
          next: 'fa fa-angle-right',
          last: 'fa fa-angle-double-right',
      }
  }
}