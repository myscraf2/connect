<?php

namespace Database\Seeders;

use App\Models\Webinar;
use Illuminate\Database\Seeder;

class WebinarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        Webinar::create([
            'title'         => 'Historia de la Psiquiatría de las Pandemias: Hechos Históricos y Hombres de Ciencia que las Vencieron: Influenza Española',
            'speaker'       => 'Dra. Irma Corlay Noriega',
            'start_at'      => '2020-12-10 00:00:00',
            'end_at'        => '2020-12-10 23:59:59',
            'url'           => 'https://playproducciones.com.mx/videos_connect/Talleres%20de%20Vanguardia_Historia%20de%20las%20Epidemias_Malaria_03SEP20_c1.mp4',
            'tags'          => ['pandemia', 'psiquiatría', 'historia', 'influenza española'],
        ]);

        Webinar::create([
            'title'         => 'FORO INTERNACIONAL APOPHARMA EN SNC',
            'speaker'       => '',
            'start_at'      => '2021-02-09 00:00:00',
            'end_at'        => '2021-02-18 23:59:59',
            'url'           => '',
            'tags'          => ['foro', 'internacional', 'apopharma', 'snc'],
        ]);
    }
}
