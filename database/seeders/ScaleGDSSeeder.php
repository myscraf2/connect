<?php

namespace Database\Seeders;

use App\Enums\ScaleQuestionType;
use App\Enums\ScaleType;
use App\Models\Scale;
use App\Models\ScaleAnswer;
use App\Models\ScaleQuestion;
use Illuminate\Database\Seeder;

class ScaleGDSSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $folstein = Scale::create([
            'type'          => ScaleType::getValue('alzheimer'),
            'name'          => 'gds',
            'title'         => 'Geriátrica de Yesavage',
            'instructions'  => '',
            'copyright'     => 'Adaptado de: Sheikh JI, Yesavage JA. Geriatric depression scale (gds): recent evidence and development of a shorterversion. In: Brink TL, eds. Clinical Gerontology: A Guide to Assessment and Intervention. New York: Haworth, 1986.',
            'banner_url'    => '/images/banners/Banner_Menural_1920x150.jpg',
            'notes'         => '0 a 5 puntos= normal, 6 a 9 puntos = depresión leve, > 10 puntos = depresión establecida.'
        ]);

        $q = ScaleQuestion::create([
            'scale_id'      => $folstein->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '1. ¿Está satisfecho/a con su vida?'           
        ]);

        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => 'Si',
            'value'             => '0',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => 'No',
            'value'             => '1',
        ]);

        $q = ScaleQuestion::create([
            'scale_id'      => $folstein->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '2. ¿Ha renunciado a muchas actividades?'           
        ]);

        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => 'Si',
            'value'             => '1',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => 'No',
            'value'             => '0',
        ]);

        $q = ScaleQuestion::create([
            'scale_id'      => $folstein->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '3. ¿Siente que su vida esta vacía?'           
        ]);

        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => 'Si',
            'value'             => '1',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => 'No',
            'value'             => '0',
        ]);

        $q = ScaleQuestion::create([
            'scale_id'      => $folstein->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '4. ¿Se encuentra a menudo aburrido/a?'           
        ]);

        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => 'Si',
            'value'             => '1',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => 'No',
            'value'             => '0',
        ]);

        $q = ScaleQuestion::create([
            'scale_id'      => $folstein->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '5. ¿Tiene a menudo buen ánimo?'           
        ]);

        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => 'Si',
            'value'             => '0',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => 'No',
            'value'             => '1',
        ]);

        $q = ScaleQuestion::create([
            'scale_id'      => $folstein->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '6. ¿Teme que algo malo le pase?'           
        ]);

        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => 'Si',
            'value'             => '1',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => 'No',
            'value'             => '0',
        ]);

        $q = ScaleQuestion::create([
            'scale_id'      => $folstein->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '7. ¿Se siente feliz muchas veces?'           
        ]);

        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => 'Si',
            'value'             => '0',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => 'No',
            'value'             => '1',
        ]);

        $q = ScaleQuestion::create([
            'scale_id'      => $folstein->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '8. ¿Se siente a menudo abandonado/a?'           
        ]);

        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => 'Si',
            'value'             => '1',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => 'No',
            'value'             => '0',
        ]);

        $q = ScaleQuestion::create([
            'scale_id'      => $folstein->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '9. ¿Prefiere quedarse en casa a salir?'           
        ]);

        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => 'Si',
            'value'             => '1',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => 'No',
            'value'             => '0',
        ]);

        $q = ScaleQuestion::create([
            'scale_id'      => $folstein->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '10. ¿Cree tener más problemas de memoria que la mayoría de la gente?'           
        ]);

        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => 'Si',
            'value'             => '1',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => 'No',
            'value'             => '0',
        ]);

        $q = ScaleQuestion::create([
            'scale_id'      => $folstein->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '11. ¿Piensa que es maravilloso vivir?'           
        ]);

        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => 'Si',
            'value'             => '0',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => 'No',
            'value'             => '1',
        ]);

        $q = ScaleQuestion::create([
            'scale_id'      => $folstein->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '12. ¿Le cuesta iniciar nuevos proyectos?'           
        ]);

        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => 'Si',
            'value'             => '1',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => 'No',
            'value'             => '0',
        ]);

        $q = ScaleQuestion::create([
            'scale_id'      => $folstein->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '13.¿Se siente lleno/a de energía?'           
        ]);

        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => 'Si',
            'value'             => '0',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => 'No',
            'value'             => '1',
        ]);

        $q = ScaleQuestion::create([
            'scale_id'      => $folstein->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '14. ¿Siente que su situación es desesperada?'           
        ]);

        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => 'Si',
            'value'             => '1',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => 'No',
            'value'             => '0',
        ]);

        $q = ScaleQuestion::create([
            'scale_id'      => $folstein->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '15. ¿Cree que mucha gente está mejor que usted?'           
        ]);

        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => 'Si',
            'value'             => '1',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => 'No',
            'value'             => '0',
        ]);
    }
}
