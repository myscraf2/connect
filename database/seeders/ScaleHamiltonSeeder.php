<?php

namespace Database\Seeders;

use App\Enums\ScaleQuestionType;
use App\Enums\ScaleType;
use App\Models\Scale;
use App\Models\ScaleAnswer;
use App\Models\ScaleQuestion;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ScaleHamiltonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Schema::disableForeignKeyConstraints();
        //    DB::table('scales')->truncate();
        //Schema::enableForeignKeyConstraints();

        $hamilton = Scale::create([
            'type'          => ScaleType::getValue('anxiety'),
            'name'          => 'hamilton',
            'title'         => 'Hamilton',
            'instructions'  => 'Esta escala especifica la severidad de los síntomas ansiosos en aquellos pacientes diagnosticados con alguno de los trastornos de ansiedad. En su diseño, la escala es precedida de unas breves instrucciones para el médico o el entrevistador en las que se precisa el rango de puntuación según la intensidad de los síntomas a saber:',
            'copyright'     => 'Guía de Práctica Clínica Diagnóstico y Tratamiento de los Trastornos de Ansiedad en el Adulto, México; Secretaría de Salud, 2010',
            'banner_url'    => '/images/banners/Banner_Apolazam_1920x150.jpg',
            'notes'         => 'No existen puntos de corte para distinguir población con y sin trastornos de ansiedad, dado que su calificación es de 0 a 56 puntos, el resultado debe interpretarse cualitativamente en términos de
            intensidad y de ser posible diferenciar entre la ansiedad psíquica (ítems 1, 2, 3, 4, 5, 6 y 14 ) y la ansiedad somática ( items 7, 8, 9, 10, 11, 12 y 13)<div class="text-center mt-2">Una puntuación mayor o igual a 15 corresponde a ansiedad moderada / grave (amerita tratamiento)<br>- Una puntuación de 6 a 14 corresponde a ansiedad leve<br>- Una puntuación de 0 a 5 corresponde a ausencia o remisión del trastorno</div>'
        ]);

        //Questions
        ScaleQuestion::create([
            'scale_id'      => $hamilton->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '1.- Humor ansioso',
            'subtitle'      => 'Inquietud, espera de lo peor, aprehensión, (anticipación temerosa), irritabilidad',
        ]);

        ScaleQuestion::create([
            'scale_id'      => $hamilton->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '2.- Tensión',
            'subtitle'      => 'Sensación de tensión, fatigabilidad, sobresaltos, llanto fácil, temblor, sensación de no poder quedarse en un solo lugar, incapacidad de relajarse',
        ]);

        ScaleQuestion::create([
            'scale_id'      => $hamilton->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '3.- Miedos',
            'subtitle'      => 'A la oscuridad, a la gente desconocida, a quedarse solo, a los animales, al tráfico, a la multitud',
        ]);

        ScaleQuestion::create([
            'scale_id'      => $hamilton->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '4.- Insomnio',
            'subtitle'      => 'Dificultad para conciliar el sueño. Sueño interrumpido, sueño no satisfactorio con cansancio al despertar, sueños penosos, pesadillas, terrores nocturnos.',
        ]);

        ScaleQuestion::create([
            'scale_id'      => $hamilton->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '5.- Funciones intelectuales',
            'subtitle'      => 'Dificultad en la concentración, mala memoria.',
        ]);

        ScaleQuestion::create([
            'scale_id'      => $hamilton->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '6.- Humor depresivo',
            'subtitle'      => 'Falta de interés, no disfrutar ya con los pasatiempos, tristeza, insomnio de madrugada, variaciones de humor durante el día.',
        ]);

        ScaleQuestion::create([
            'scale_id'      => $hamilton->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '7.- Síntomas somáticos (musculares)',
            'subtitle'      => 'Dolores y cansancio muscular, rigidez, sacudidas mioclónicas, chirrido de dientes, voz poco firme, tono muscular aumentado.',
        ]);

        ScaleQuestion::create([
            'scale_id'      => $hamilton->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '8.- Síntomas somáticos generales (sensoriales)',
            'subtitle'      => 'Zumbido de oídos, visión borrosa, sofocos o escalosfríos, sensación de debilidad, sensación de hormigueo',
        ]);

        ScaleQuestion::create([
            'scale_id'      => $hamilton->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '9.-Síntomas cardiovasculares',
            'subtitle'      => 'Taquicardia, palpitaciones, dolores en el pecho, latidos vasculares, sensación de desmayo, extrasístoles',
        ]);

        ScaleQuestion::create([
            'scale_id'      => $hamilton->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '10.- Síntomas respiratorios',
            'subtitle'      => 'Peso u opresión torácica, sensación de ahogo, suspiros, disnea',
        ]);

        ScaleQuestion::create([
            'scale_id'      => $hamilton->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '11.-Síntomas gastrointestinales',
            'subtitle'      => 'Dificultad para meteorismo, dolor náusea, vómitos, sensación de estómago vacío, pérdida de peso, estreñimiento.',
        ]);

        ScaleQuestion::create([
            'scale_id'      => $hamilton->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '12.-Síntomas genitourinarios',
            'subtitle'      => 'Micciones frecuentes, urgencia de micción, amenorrea, menorragia, desarrollo de frigidez, eyaculación precoz, pérdida del apetito sexual, disfunción eréctil',
        ]);

        ScaleQuestion::create([
            'scale_id'      => $hamilton->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '13.- Síntomas del sistema nervioso vegetativo',
            'subtitle'      => 'Boca seca, accesos de rubor, palidez, transpiración excesiva, vértigo, cefalea por tensión, erectismo piloso.',
        ]);

        ScaleQuestion::create([
            'scale_id'      => $hamilton->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '14.- Comportamiento agitado durante la entrevista',
            'subtitle'      => 'Agitado, inquieto o dando vueltas, manos temblorosas, ceño fruncido, facies tensa, suspiros o respiración agitada, palidez, tragar saliva, eructos, rápidos movimientos de los tendones, midriasis, exoftalmos.',
        ]);

        //Answers
        $questions = ScaleQuestion::where('scale_id', $hamilton->id)->get();
        foreach ($questions as $value) {
            ScaleAnswer::create([
                'scale_question_id' => $value->id,
                'name'              => 'ausente',
                'value'             => 0,
            ]);
            ScaleAnswer::create([
                'scale_question_id' => $value->id,
                'name'              => 'leve',
                'value'             => 1,
            ]);
            ScaleAnswer::create([
                'scale_question_id' => $value->id,
                'name'              => 'moderado',
                'value'             => 2,
            ]);
            ScaleAnswer::create([
                'scale_question_id' => $value->id,
                'name'              => 'severo',
                'value'             => 3,
            ]);
            ScaleAnswer::create([
                'scale_question_id' => $value->id,
                'name'              => 'grave o totalmente incapacitado',
                'value'             => 4,
            ]);
           
        }       

        
    }
}
