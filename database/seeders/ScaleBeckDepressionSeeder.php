<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Enums\ScaleQuestionType;
use App\Enums\ScaleType;
use App\Models\Scale;
use App\Models\ScaleAnswer;
use App\Models\ScaleQuestion;

class ScaleBeckDepressionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $beck = Scale::create([
            'type'          => ScaleType::getValue('depression'),
            'name'          => 'beck_depression',
            'title'         => 'Beck',
            'instructions'  => 'En este cuestionario aparecen varios grupos de afirmaciones. Por favor,
            lea con atención cada una. A continuación, señale cuál de las afirmaciones de cada grupo describe mejor cómo se ha sentido durante esta última semana, incluido en el día de hoy.',
            'copyright'     => 'Diagnóstico y Tratamiento del Trastorno depresivo en el adulto. México: Secretaría de Salud; 2 de diciembre  de 2015',
            'banner_url'    => '/images/banners/Banner_ApoOxpar_1920x250.jpg',
            'notes'         => 'Guía para la interpretación del inventario de la depresión de Beck:<br>Puntuación nivel de depresión*<br>1-10 ............ Estos altibajos son considerados normales.<br>11-16 .......... Leve perturbación del estado de ánimo.<br>17-20 .......... Estados de depresión intermitentes<br>21-30 .......... Depresión moderada<br>31-40 .......... Depresión grave<br>+ 40 ............ Depresión extrema<br>*Una puntuación persistente de 17 o más indica que puede necesitar ayuda profesional.'
        ]);

        $questions = [
            '1).' => [
                'No me siento triste',
                'Me siento triste.',
                'Me siento triste continuamente y no puedo dejar de estarlo',
                'Me siento tan triste o tan desgraciado que no puedo soportarlo'
            ],
            '2).' => [
                'No me siento especialmente desanimado respecto al futuro.',
                'Me siento desanimado respecto al futuro.',
                'Siento que no tengo que esperar nada.',
                'Siento que el futuro es desesperanzador y las cosas no mejorarán'
            ],
            '3).' => [
                'No me siento fracasado.',
                'Creo que he fracasado más que la mayoría de las personas.',
                'Cuando miro hacia atrás, sólo veo fracaso tras fracaso.',
                'Me siento una persona totalmente fracasada'
            ],
            '4).' => [
                'Las cosas me satisfacen tanto como antes.',
                'No disfruto de las cosas tanto como antes.',
                'Ya no obtengo una satisfacción auténtica de las cosas.',
                'Estoy insatisfecho o aburrido de todo.'
            ],
            '5).' => [
                'No me siento especialmente culpable.',
                'Me siento culpable en bastantes ocasiones.',
                'Me siento culpable en la mayoría de las ocasiones.',
                'Me siento culpable constantemente.'
            ],
            '6).' => [
                'No creo que esté siendo castigado.',
                'Me siento como si fuese a ser castigado.',
                'Espero ser castigado.',
                'Siento que estoy siendo castigado.'
            ],
            '7).' => [
                'No estoy decepcionado de mí mismo.',
                'Estoy decepcionado de mí mismo.',
                'Me da vergüenza de mí mismo.',
                'Me detesto.'
            ],
            '8).' => [
                'No me considero peor que cualquier otro.',
                'Me autocritico por mis debilidades o por mis errores.',
                'Continuamente me culpo por mis faltas.',
                'Me culpo por todo lo malo que sucede.'
            ],
            '9).' => [
                'No tengo ningún pensamiento de suicidio.',
                'A veces pienso en suicidarme, pero no lo cometería.',
                'Desearía suicidarme.',
                'Me suicidaría si tuviese la oportunidad.'
            ],
            '10).' => [
                'No lloro más de lo que solía llorar.',
                'Ahora lloro más que antes.',
                'Lloro continuamente.',
                'Antes era capaz de llorar, pero ahora no puedo, incluso aunque quiera.'
            ],
            '11).' => [
                'No estoy más irritado de lo normal en mí.',
                'Me molesto o irrito más fácilmente que antes.',
                'Me siento irritado continuamente.',
                'No me irrito absolutamente nada por las cosas que antes solían irritarme.'
            ],
            '12).' => [
                'No he perdido el interés por los demás.',
                'Estoy menos interesado en los demás que antes.',
                'He perdido la mayor parte de mi interés por los demás.',
                'He perdido todo el interés por los demás.'
            ],
            '13).' => [
                'Tomo decisiones más o menos como siempre he hecho.',
                'Evito tomar decisiones más que antes.',
                'Tomar decisiones me resulta mucho más difícil que antes.',
                'Ya me es imposible tomar decisiones.'
            ],
            '14).' => [
                'No creo tener peor aspecto que antes.',
                'Me temo que ahora parezco más viejo o poco atractivo.',
                'Creo que se han producido cambios permanentes en mi aspecto que me hacen parecer poco atractivo.',
                'Creo que tengo un aspecto horrible.'
            ],
            '15).' => [
                'Trabajo igual que antes.',
                'Me cuesta un esfuerzo extra comenzar a hacer algo.',
                'Tengo que obligarme mucho para hacer algo.',
                'No puedo hacer nada en absoluto.'
            ],
            '16).' => [
                'Duermo tan bien como siempre.',
                'No duermo tan bien como antes.',
                'Me despierto una o dos horas antes de lo habitual y me resulta difícil volver a dormir.',
                'Me despierto varias horas antes de lo habitual y no puedo volverme a dormir.'
            ],
            '17).' => [
                'No me siento más cansado de lo normal.',
                'Me canso más fácilmente que antes.',
                'Me canso en cuanto hago cualquier cosa.',
                'Estoy demasiado cansado para hacer nada.'
            ],
            '18).' => [
                'Mi apetito no ha disminuido.',
                'No tengo tan buen apetito como antes.',
                'Ahora tengo mucho menos apetito.',
                'He perdido completamente el apetito.'
            ],
            '19).' => [
                'Últimamente he perdido poco peso o no he perdido nada.',
                'He perdido más de 2 kilos y medio.',
                'He perdido más de 4 kilos.',
                'He perdido más de 7 kilos.'
            ],
            '20).' => [
                'No estoy preocupado por mi salud más de lo normal.',
                'Estoy preocupado por problemas físicos como dolores, molestias, malestar de estómago o estreñimiento.',
                'Estoy preocupado por mis problemas físicos y me resulta difícil pensar algo más.',
                'Estoy tan preocupado por mis problemas físicos que soy incapaz de pensar en cualquier cosa.'
            ],
            '21).' => [
                'No he observado ningún cambio reciente en mi interés.',
                'Estoy menos interesado por el sexo que antes.',
                'Estoy mucho menos interesado por el sexo.',
                'He perdido totalmente mi interés por el sexo.'
            ],
        ];

        foreach ($questions as $key => $value) {
            $q = ScaleQuestion::create([
                'scale_id'      => $beck->id,
                'type'          => ScaleQuestionType::getValue('radio'),
                'title'         => $key,
            ]);
            foreach ($value as $key => $value) {
                ScaleAnswer::create([
                    'scale_question_id' => $q->id,
                    'name'              => $value,
                    'value'             => $key,
                ]);
            }
        }  
    }
}
