<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Enums\ScaleQuestionType;
use App\Enums\ScaleType;
use App\Models\Scale;
use App\Models\ScaleAnswer;
use App\Models\ScaleQuestion;

class ScaleBeckAnxietySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hamilton = Scale::create([
            'type'          => ScaleType::getValue('anxiety'),
            'name'          => 'beck_anxiety',
            'title'         => 'Beck (BAI)',
            'instructions'  => 'Abajo hay una lista que contiene los síntomas más comunes de la ansiedad. Lea cuidadosamente cada afirmación. Indique cuánto le ha molestado cada síntoma durante la última semana, inclusive hoy, marcando con una X según la intensidad de la molestia',
            'copyright'     => 'Fuente: Beck AT, Steer RA. Relationship between the Beck Anxiety Inventory and the Hamilton Anxiety Rating Scale with anxious outpatients. 1991.',
            'banner_url'    => '/images/banners/Banner_Apolazam_1920x150.jpg',
            'notes'         => 'No existe punto de corte aceptado para distinguir entre población normal y ansiedad. La puntuación media en pacientes con ansiedad es de 25.7 y en sujetos normales es de 15.8<br>La puntuación: De 0 a 5 puntos = ansiedad mínima; de 6 a 15 puntos = ansiedad leve; de 16 a 30 puntos = ansiedad moderada; y de 31 a 63 puntos = ansiedad severa. El criterio para considerar la ansiedad como clínicamente relevante es obtener 16 puntos o más.'
        ]);

        //Questions
        ScaleQuestion::create([
            'scale_id'      => $hamilton->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '1.- Entumecimiento, hormigueo',
        ]);

        ScaleQuestion::create([
            'scale_id'      => $hamilton->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '2.- Sentir oleadas de calor ( bochorno )',
        ]);

        ScaleQuestion::create([
            'scale_id'      => $hamilton->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '3.- Debilitamiento de las piernas',
        ]);

        ScaleQuestion::create([
            'scale_id'      => $hamilton->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '4.- Dificultad para relajarse',
        ]);

        ScaleQuestion::create([
            'scale_id'      => $hamilton->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '5.- Miedo a que pase lo peor',
        ]);

        ScaleQuestion::create([
            'scale_id'      => $hamilton->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '6.- Sensación de mareo',
        ]);

        ScaleQuestion::create([
            'scale_id'      => $hamilton->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '7.- Opresión en el pecho, o latidos acelerados',
        ]);

        ScaleQuestion::create([
            'scale_id'      => $hamilton->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '8.- Inseguridad',
        ]);

        ScaleQuestion::create([
            'scale_id'      => $hamilton->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '9.- Terror',
        ]);

        ScaleQuestion::create([
            'scale_id'      => $hamilton->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '10.- Nerviosismo',
        ]);

        ScaleQuestion::create([
            'scale_id'      => $hamilton->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '11.- Sensación de ahogo',
        ]);

        ScaleQuestion::create([
            'scale_id'      => $hamilton->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '12.- Manos temblorosas',
        ]);

        ScaleQuestion::create([
            'scale_id'      => $hamilton->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '13.- Cuerpo tembloroso',
        ]);

        ScaleQuestion::create([
            'scale_id'      => $hamilton->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '14.- Miedo a perder el control',
        ]);

        ScaleQuestion::create([
            'scale_id'      => $hamilton->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '15.- Dificultad para respirar',
        ]);

        ScaleQuestion::create([
            'scale_id'      => $hamilton->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '16.- Miedo a morir',
        ]);

        ScaleQuestion::create([
            'scale_id'      => $hamilton->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '17.- Asustado',
        ]);

        ScaleQuestion::create([
            'scale_id'      => $hamilton->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '18.- Indigestión o malestar estomacal',
        ]);

        ScaleQuestion::create([
            'scale_id'      => $hamilton->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '19.- Debilidad',
        ]);

        ScaleQuestion::create([
            'scale_id'      => $hamilton->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '20.- Ruborizarse, sonrojamiento',
        ]);

        ScaleQuestion::create([
            'scale_id'      => $hamilton->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '21.- Sudoración no debida al calor',
        ]);

        //Answers
        $questions = ScaleQuestion::where('scale_id', $hamilton->id)->get();
        foreach ($questions as $value) {
            ScaleAnswer::create([
                'scale_question_id' => $value->id,
                'name'              => 'Poco o nada',
                'value'             => 0,
            ]);
            ScaleAnswer::create([
                'scale_question_id' => $value->id,
                'name'              => 'Más o menos',
                'value'             => 1,
            ]);
            ScaleAnswer::create([
                'scale_question_id' => $value->id,
                'name'              => 'Moderadamente',
                'value'             => 2,
            ]);
            ScaleAnswer::create([
                'scale_question_id' => $value->id,
                'name'              => 'Severamente',
                'value'             => 3,
            ]);
           
        }       
    }
}
