<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
            DB::table('roles')->truncate();
            DB::table('permissions')->truncate();
            DB::table('role_permissions')->truncate();
            DB::table('model_roles')->truncate();
            DB::table('model_permissions')->truncate();
            DB::table('users')->truncate();
        Schema::enableForeignKeyConstraints();

        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        $permissions = ['users','roles','permissions','webinars','courses','modules','scales'];

        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission . '.create']);
            Permission::create(['name' => $permission . '.read']);
            Permission::create(['name' => $permission . '.update']);
            Permission::create(['name' => $permission . '.delete']);
        }

        //Admin role
        $role = Role::create(['name' => 'admin', 'description' => 'Administrador']);
        $role->givePermissionTo('users.create');
        $role->givePermissionTo('users.read');
        $role->givePermissionTo('users.update');
        $role->givePermissionTo('users.delete');  
        $role->givePermissionTo('roles.create');
        $role->givePermissionTo('roles.read');
        $role->givePermissionTo('roles.update');
        $role->givePermissionTo('roles.delete');  
        $role->givePermissionTo('permissions.create');
        $role->givePermissionTo('permissions.read');
        $role->givePermissionTo('permissions.update');
        $role->givePermissionTo('permissions.delete');       
        $role->givePermissionTo('webinars.create');
        $role->givePermissionTo('webinars.read');
        $role->givePermissionTo('webinars.update');
        $role->givePermissionTo('webinars.delete');
        $role->givePermissionTo('courses.create');
        $role->givePermissionTo('courses.read');
        $role->givePermissionTo('courses.update');
        $role->givePermissionTo('courses.delete');
        $role->givePermissionTo('modules.create');
        $role->givePermissionTo('modules.read');
        $role->givePermissionTo('modules.update');
        $role->givePermissionTo('modules.delete');
    }
}
