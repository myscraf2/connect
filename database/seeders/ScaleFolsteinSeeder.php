<?php

namespace Database\Seeders;

use App\Enums\ScaleQuestionType;
use App\Enums\ScaleType;
use App\Models\Scale;
use App\Models\ScaleAnswer;
use App\Models\ScaleQuestion;
use Illuminate\Database\Seeder;

class ScaleFolsteinSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $folstein = Scale::create([
            'type'          => ScaleType::getValue('alzheimer'),
            'name'          => 'folstein',
            'title'         => 'Folstein',
            'instructions'  => 'Lea cada una de las siguientes preguntas a su paciente, anote un punto por cada respuesta correcta, sume los puntos y seleccione la opción que corresponda al resultado.',
            'copyright'     => 'Modificado de: Folstein MF, Folstein SE, MaHugh PR. Mini-Mental State: A practical method for grading the cognitive state of patientsfor the clinician. Journal of Psychiatric Research, 1975;19:189-98 ',
            'banner_url'    => '/images/banners/Banner_Menural_1920x150.jpg',
            'notes'         => 'Punto de corte: 24-30 puntos = NORMAL. Grado de deterioro cognoscitivo: 19-23 = LEVE; 14 – 18 = MODERADO; Menor a 14 = GRAVE'
        ]);

        $q = ScaleQuestion::create([
            'scale_id'      => $folstein->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '1.- Orientación en el tiempo y espacio:<br>¿Qué día de la semana es hoy?<br>¿Cuál es el año?<br>¿Cuál es el mes?<br>¿Cuál es el día?<br>¿Cuál es la estación del año?'           
        ]);

        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '0 puntos',
            'value'             => '0',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '1 punto',
            'value'             => '1',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '2 puntos',
            'value'             => '2',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '3 puntos',
            'value'             => '3',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '4 puntos',
            'value'             => '4',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '5 puntos',
            'value'             => '5',
        ]);

        $q = ScaleQuestion::create([
            'scale_id'      => $folstein->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '2.- Orientación en el tiempo y espacio:<br>¿En dónde estamos ahora?<br>¿En qué piso estamos?<br>¿En qué ciudad estamos?<br>¿En qué Estado vivimos?<br>¿En qué país estamos?'           
        ]);

        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '0 puntos',
            'value'             => '0',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '1 punto',
            'value'             => '1',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '2 puntos',
            'value'             => '2',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '3 puntos',
            'value'             => '3',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '4 puntos',
            'value'             => '4',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '5 puntos',
            'value'             => '5',
        ]);

        $q = ScaleQuestion::create([
            'scale_id'      => $folstein->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '3.- Fijación<br>Le voy a decir 3 palabras, cuando yo los termine quiero que por favor Usted los repita”
            (Anote un punto cada vez que la palabra sea correcta).'           
        ]);

        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '0 puntos',
            'value'             => '0',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '1 punto',
            'value'             => '1',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '2 puntos',
            'value'             => '2',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '3 puntos',
            'value'             => '3',
        ]);

        $q = ScaleQuestion::create([
            'scale_id'      => $folstein->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '4.- Concentración y Cálculo<br>Le voy a pedir que reste de 7 en 7 a partir del 100." (Anote un punto cada vez que la diferencia sea correcta aunque la anterior fuera incorrecta).'           
        ]);

        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '0 puntos',
            'value'             => '0',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '1 punto',
            'value'             => '1',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '2 puntos',
            'value'             => '2',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '3 puntos',
            'value'             => '3',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '4 puntos',
            'value'             => '4',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '5 puntos',
            'value'             => '5',
        ]);

        $q = ScaleQuestion::create([
            'scale_id'      => $folstein->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '5.- Memoria<br>¿Recuerda usted las tres palabras que le dije antes? Dígalas'           
        ]);

        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '0 puntos',
            'value'             => '0',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '1 punto',
            'value'             => '1',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '2 puntos',
            'value'             => '2',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '3 puntos',
            'value'             => '3',
        ]);

        $q = ScaleQuestion::create([
            'scale_id'      => $folstein->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '6.- Lenguaje y Construcción<br>¿Qué es esto? (Mostrar un reloj).<br>¿y esto? (Mostrar un bolígrafo).'          
        ]);

        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '0 puntos',
            'value'             => '0',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '1 punto',
            'value'             => '1',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '2 puntos',
            'value'             => '2',
        ]);

        $q = ScaleQuestion::create([
            'scale_id'      => $folstein->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '7.- Lenguaje y Construcción<br>Repita la siguiente frase después de mí: - "ni si, ni no, ni pero"'          
        ]);

        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '0 puntos',
            'value'             => '0',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '1 punto',
            'value'             => '1',
        ]);


        $q = ScaleQuestion::create([
            'scale_id'      => $folstein->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '8.- Lenguaje y Construcción<br>Le voy a dar algunas instrucciones. Por favor sígalas en el orden en que se las voy a decir.
            1. Tome el papel con la mano izquierda, 2. dóblelo por la mitad y 3. póngalo en el suelo<br>(Anote un punto por cada orden bien ejecutada).'          
        ]);

        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '0 puntos',
            'value'             => '0',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '1 punto',
            'value'             => '1',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '2 punto',
            'value'             => '2',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '3 punto',
            'value'             => '3',
        ]);

        $q = ScaleQuestion::create([
            'scale_id'      => $folstein->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '9.- Lenguaje y Construcción<br>Lea esto y haga lo que dice: "Cierre los ojos"'          
        ]);

        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '0 puntos',
            'value'             => '0',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '1 punto',
            'value'             => '1',
        ]);


        $q = ScaleQuestion::create([
            'scale_id'      => $folstein->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '10.- Lenguaje y Construcción<br>Quiero que por favor escriba una frase que diga un mensaje'          
        ]);

        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '0 puntos',
            'value'             => '0',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '1 punto',
            'value'             => '1',
        ]);


        $q = ScaleQuestion::create([
            'scale_id'      => $folstein->id,
            'type'          => ScaleQuestionType::getValue('radio'),
            'title'         => '11.- Lenguaje y Construcción<br>Copie este dibujo',
            'subtitle'      => 'Deberá utilizar anteojos si los necesita habitualmente. (cada pentágono debe tener 5 lados y 5 vértices y la intersección forma un diamante)<br>Nota: tanto la frase “Cierre los ojos” como los pentágonos conviene tenerlos en tamaño suficiente para poder ser leídos con facilidad por el paciente.', 
            'image_url'     => '/images/scales/folstein.png'      
        ]);

        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '0 puntos',
            'value'             => '0',
        ]);
        ScaleAnswer::create([
            'scale_question_id' => $q->id,
            'name'              => '1 punto',
            'value'             => '1',
        ]);
        
        
    }
}
