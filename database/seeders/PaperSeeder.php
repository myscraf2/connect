<?php

namespace Database\Seeders;

use App\Models\Paper;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class PaperSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Schema::disableForeignKeyConstraints();
            DB::table('papers')->truncate();
        Schema::enableForeignKeyConstraints();

        Paper::create([
            'title'     => '<b>Impact</b> of <b>SARS-CoV-2</b> infection on <b>neurodegenerative</b> and <b>neuropsychiatric diseases</b>',
            'authors'   => 'Serrano-Castro PJ, Estivill-Torrús G, Cabezudo-García P, Reyes-Bueno JA, Ciano Petersen N, Aguilar-Castillo MJ, et al.',
            'source'    => 'Neurología. 2020;35(4):245—251',
            'copyright' => '2173-5808/© 2020 Sociedad Española de Neurología. Published by Elsevier España, S.L.U. This is an open access article under the CC BY-NC-ND license (http://creativecommons.org/licenses/by-nc-nd/4.0/).',
            'url'  => 'https://aconnect.sfo2.digitaloceanspaces.com/pdfs/Impact of SARS CoV 2 infection on neurodegenerative and neuropsychiatric diseases.pdf',
        ]);

        Paper::create([
            'title'     => 'A commentary on <b>the efficacy of olanzapine</b> for the <b>treatment of schizophrenia</b>: the past, present and future',
            'authors'   => 'Leslie Citrome, Joseph P McEvoy, Mark S Todtenkopf, David McDonnell, Peter J Weiden',
            'source'    => 'Neuropsychiatric Disease and Treatment 2019:15',
            'copyright' => '© 2019 Citrome et al. This work is published and licensed by Dove Medical Press Limited. The full terms of this license are available at https://www.dovepress.com/terms.php and incorporate the Creative Commons Attribution – Non Commercial (unported, v3.0) License (http://creativecommons.org/licenses/by-nc/3.0/)',
            'url'  => 'https://aconnect.sfo2.digitaloceanspaces.com/pdfs/commentary%20on%20efficacy%20olanzapine%20treatment%202019.pdf',
        ]);

        Paper::create([
            'title'     => 'Changing prevalence and treatment of <b>depression among older people</b> over two decades',
            'authors'   => 'Antony Arthur, George M. Savva, Linda E. Barnes, Ayda Borjian-Boroojeny, Tom Dening, Carol Jagger,
            Fiona E. Matthews, Louise Robinson, Carol Brayne and the Cognitive Function and Ageing Studies
            Collaboration',
            'source'    => 'The British Journal of Psychiatry (2020) 216, 49–54',
            'copyright' => '© The Royal College of Psychiatrists 2019. This is an Open Accessarticle, distributed under the terms of the Creative Commons Attribution licence (http://creativecommons.org/licenses/by/4.0/), which permits unrestricted re-use, distribution, and reproduction in any medium, provided the original work is properly cited.',
            'url'  => 'https://aconnect.sfo2.digitaloceanspaces.com/pdfs/changing_prevalence_and_treatment_of_depression_among_older_people_over_two_decades.pdf',
        ]);

        Paper::create([
            'title'     => 'Association between <b>childhood trauma</b> and multimodal <b>early-onset hallucinations</b>',
            'authors'   => 'François Medjkane, Charles-Edouard Notredame, Lucie Sharkey, Fabien D’Hondt, Guillaume Vaiva and Renaud Jardri',
            'source'    => 'The British Journal of Psychiatry (2020) 216, 156–158',
            'copyright' => '© The Author(s) 2020. This is an Open Access article, distributed under the terms of the Creative Commons Attribution licence (http://creativecommons.org/licenses/by/4.0/), which permits unrestricted re-use, distribution, and reproduction in any medium, provided the original work is properly cited.',
            'url'  => 'https://aconnect.sfo2.digitaloceanspaces.com/pdfs/association_between_childhood_trauma_and_multimodal_earlyonset_hallucinations.pdf',
        ]);

        Paper::create([
            'title'     => 'Computerized <b>cognitive performance assessments</b> in the Brooklyn <b>Cognitive Impairments</b> in Health Disparities Pilot Study',
            'authors'   => 'Kerry Adlera, Samuel Applea, Alison Friedlanderb, Frank Chengc, Carl Cohend, Steven R. Levinee, Michael Lucchesif, Paul Maruffc, Michael Reinhardtd, Margaret Salmieria, David Stevensa, Sarah Weingasta, Deborah Gustafsone',
            'source'    => 'Alzheimer’s & Dementia 15 (2019) 1420-1426',
            'copyright' => '© 2019 The Authors. Published by Elsevier Inc. on behalf of the Alzheimer’s Association. This is an open access article under the CC BY-NC-ND license (http://creativecommons.org/licenses/by-nc-nd/4.0/).',
            'url'  => 'https://aconnect.sfo2.digitaloceanspaces.com/pdfs/Computerized%20cognitive%20performance%20assessments%20in%20the%20Brooklyn.pdf',
        ]);

        Paper::create([
            'title'     => '<b>Dementia</b> and Antisocial Behaviors',
            'authors'   => 'Michiho Sodenaga, Shin’ya Tayoshi, Mioto Maedomari, Chiaki Hashimoto, Sachiko Tsukahara, Masanori Tadokoro and Koji Hori',
            'source'    => 'Journal of Alzheimer’s Disease & Parkinsonism 2020, 10:1',
            'copyright' => '© 2020 Sodenaga M, et al. This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.',
            'url'  => 'https://aconnect.sfo2.digitaloceanspaces.com/pdfs/dementia-and-antisocial-behaviors.pdf',
        ]);

        Paper::create([
            'title'     => 'Effectiveness of a <b>volunteer befriending programme</b> for <b>patients</b> with <b>schizophrenia</b>: randomised controlled trial',
            'authors'   => 'Stefan Priebe, Agnes Chevalier, Thomas Hamborg, Eoin Golden, Michael King and Nancy Pistrang',
            'source'    => 'The British Journal of Psychiatry (2019) Page 1 of 7',
            'copyright' => '©The Royal College of Psychiatrists 2019. This is an Open Access article, distributed under the terms of the Creative Commons Attribution licence (http://creativecommons.org/licenses/by/4.0/), which permits unrestricted re-use, distribution, and reproduction in any medium, provided the original work is properly cited.',
            'url'  => 'https://aconnect.sfo2.digitaloceanspaces.com/pdfs/effectiveness_of_a_volunteer_befriending_programme_for_patients_with_schizophrenia_randomised_controlled_trial.pdf',
        ]);

        Paper::create([
            'title'     => 'Seeing <b>ophthalmologic problems</b> in <b>Parkinson disease,</b> Results of a visual impairment questionnaire',
            'authors'   => 'Carlijn D.J.M. Borm, MD, Femke Visser, MD, Mario Werkmann, MD, Debbie de Graaf, MSc, Diana Putz, MD, Klaus Seppi, MD, Werner Poewe, MD, Annemarie M.M. Vlaar, MD, PhD, Carel Hoyng, MD, PhD, Bastiaan R. Bloem, MD, PhD, Thomas Theelen, MD, PhD, and Nienke M. de Vries, PhD',
            'source'    => 'Neurology® 2020;94:1-9',
            'copyright' => 'This is an open access article distributed under the terms of the Creative Commons Attribution-NonCommercial-NoDerivatives License 4.0 (CC BY-NC-ND), which permits downloading and sharing the work provided it is properly cited. The work cannot be changed in any way or used commercially without permission from the journal.',
            'url'  => 'https://aconnect.sfo2.digitaloceanspaces.com/pdfs/Articulo%20Parkinson%20opthalmologic%20problems.pdf',
        ]);

        Paper::create([
            'title'     => 'Impaired serine metabolism complements <b>LRRK2-G2019S pathogenicity</b> in <b>Parkinson Disease</b> patients',
            'authors'   => 'Sarah Louise Nickels, Jonas Walter, Silvia Bolognin, Deborah Gérard, Christian Jaeger, Xiaobing Qing, Johan Tisserand, Javier Jarazo, Kathrin Hemmer, Amy Harms, Rashi Halder, Philippe Lucarelli, Emanuel Berger, Paul M.A. Antony, Enrico Glaab, Thomas Hankemeier, Christine Klein, Thomas Sauter, Lasse Sinkkonen, Jens Christian Schwamborn',
            'source'    => 'Parkinsonism and Related Disorders 67 (2019) 48–55',
            'copyright' => '1353-8020/ © 2019 The Author(s). Published by Elsevier Ltd. This is an open access article under the CC BY-NC-ND license
            (http://creativecommons.org/licenses/BY-NC-ND/4.0/).',
            'url'  => 'https://aconnect.sfo2.digitaloceanspaces.com/pdfs/Impaired-serine-metabolism-complements-LRRK2-G2019_2019_Parkinsonism.pdf',
        ]);

        Paper::create([
            'title'     => 'Integrated <b>Community Support</b> for People with <b>Dementia</b>',
            'authors'   => 'Amberyce Ang',
            'source'    => 'Journal of Alzheimer’s Disease & Parkinsonism 2020, 10:1',
            'copyright' => '© 2020 Ang A, et al. This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited',
            'url'  => 'https://aconnect.sfo2.digitaloceanspaces.com/pdfs/integrated-community-support-for-people-with-dementia.pdf',
        ]);
    }
}
