<?php

namespace Database\Seeders;

use App\Models\News;
use App\Models\NewsCategory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class NewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Schema::disableForeignKeyConstraints();
        DB::table('news_categories')->truncate();
        DB::table('news')->truncate();
        Schema::enableForeignKeyConstraints();

        //Categories
        $mexico = NewsCategory::create([
            'title' => '<span class="text-red-600">· México</span> | Economía, entorno y COVID-19',
            'image_url' => 'https://aconnect.sfo2.digitaloceanspaces.com/news/categories/mexico.jpg'
        ]);
        $latam = NewsCategory::create([
            'title' => '<span class="text-red-600">· América Latina</span> | Economía, entorno y COVID-19',
            'image_url' => 'https://aconnect.sfo2.digitaloceanspaces.com/news/categories/latinoamerica.jpg'
        ]);
        $internacional = NewsCategory::create([
            'title' => '<span class="text-red-600">· Internacional</span> | Entorno, Productos y Regulación',
            'image_url' => 'https://aconnect.sfo2.digitaloceanspaces.com/news/categories/internacional.jpg'
        ]);
        $usa = NewsCategory::create([
            'title' => '<span class="text-red-600">· Estados Unidos</span> | Entorno, Productos y Regulación',
            'image_url' => 'https://aconnect.sfo2.digitaloceanspaces.com/news/categories/estados-unidos.jpg'
        ]);


        News::create([
            'news_category_id'  => $mexico->id,
            'title'             => 'Farmacéuticas, listas para entregar medicinas, pero Insabi no define logística',
            'url'               => 'https://www.eleconomista.com.mx/empresas/Farmaceuticas-listas-para-entregar-medicinas-pero-Insabi-no-define-logistica-20210317-0011.html',
            'publish_date'      => '2021-03-17',
            'source'            => 'Marzo 17, 2021 • El Economista',
            'content'           => 'Proveedores que abastecen 20% de la compra consolidada de medicamentos que realiza la UNOPS alertan por posibles retrasos ante la falta de lineamientos de entrega; proponen distribuir directamente el producto pero la autoridad no ha dado respuesta.',
        ]);

        News::create([
            'news_category_id'  => $mexico->id,
            'title'             => 'Gobierno aplicará 600 mil dosis al día',
            'url'               => 'https://www.eluniversal.com.mx/nacion/gobierno-aplicara-600-mil-dosis-al-dia',
            'publish_date'      => '2021-03-16',
            'source'            => 'Marzo 16, 2021 • El Universal',
            'content'           => 'Hugo López-Gatell, subsecretario de Salud, actualizó el compromiso de vacunación y aseguró que el gobierno de México llegará a aplicar hasta 600 mil dosis diarias en cuanto comiencen a llegar más embarques de biológicos.',
        ]);

        News::create([
            'news_category_id'  => $mexico->id,
            'title'             => 'Sube demanda de plástico por cubrebocas y jeringas',
            'url'               => 'https://www.eluniversal.com.mx/cartera/sube-demanda-de-plastico-por-cubrebocas-y-jeringas',
            'publish_date'      => '2021-03-16',
            'source'            => 'Marzo 16, 2021 • El Universal',
            'content'           => 'La elaboración de productos como jeringas, cubrebocas y coberturas para alimentos a causa de la pandemia de Covid-19 ha aumentado la demanda de plástico en México, donde la aplicación de vacunas subirá 35% la producción del material para inyectar.',
        ]);

        News::create([
            'news_category_id'  => $mexico->id,
            'title'             => 'Sube desorden en compra de medicamentos',
            'url'               => 'https://www.eleconomista.com.mx/opinion/Sube-desorden-en-compra-de-medicamentos-20210315-0027.html',
            'publish_date'      => '2021-03-15',
            'source'            => 'Marzo 15, 2021 • El Economista',
            'content'           => 'El inocultable desabasto de medicamentos y otros insumos de la salud en México desde hace dos años ha derivado ya en cierto conocimiento público sobre cómo opera el sector farmacéutico. Es claro que no se maneja con inventarios; que en ninguna parte del mundo hay bodegas llenas de medicamentos esperando a ver quién llega a comprarlos. Es una industria que fabrica en función de los contratos de compra firmados, porque sus productos tienen caducidad precisa y limitada y por la naturaleza de sus materias primas.',
        ]);

        News::create([
            'news_category_id'  => $mexico->id,
            'title'             => '¿Es seguro usar la vacuna COVID de AstraZeneca? Esto dice López-Gatell',
            'url'               => 'https://www.elfinanciero.com.mx/salud/es-seguro-usar-la-vacuna-covid-de-astrazeneca-esto-dice-lopez-gatell',
            'publish_date'      => '2021-03-15',
            'source'            => 'Marzo 15, 2021 • El Financiero',
            'content'           => '"El propio AstraZeneca, que está obligado a hacer un informe de farmacovigilancia muy detallado, ya ha presentado a la Agencia Europea de Medicamentos y también a la Organización Mundial de Salud el informe de lo que ha investigado y conocemos que no se encuentra que la frecuencia de los eventos trombóticos sea mayor a lo esperado en la población vacunada comparado con lo que se esperaría en el conjunto de la población. Estamos hablando de un par de decenas de eventos trombóticos en una población en la que se esperaría 750 eventos", indicó este lunes en la conferencia de prensa sobre COVID-19.',
        ]);

        News::create([
            'news_category_id'  => $mexico->id,
            'title'             => 'AMLO no descarta tercer rebrote de COVID-19 en México; pide acelerar vacunación',
            'url'               => 'https://www.razon.com.mx/mexico/amlo-descarta-tercer-rebrote-covid-19-pais-427047',
            'publish_date'      => '2021-03-15',
            'source'            => 'Marzo 15, 2021 • La Razón',
            'content'           => 'El Presidente Andrés Manuel López Obrador no descartó la posibilidad de un tercer rebrote de COVID-19 en México, por lo que se tienen que tomar las acciones necesarias para enfrentarlo con la pronta vacunación de los adultos mayores, la cual estará concluida a finales de abril.',
        ]);

        News::create([
            'news_category_id'  => $mexico->id,
            'title'             => 'Adulto mayor muere tras recibir vacuna anticovid en Azcapotzalco; descartan relación con la dosis',
            'url'               => 'https://www.milenio.com/politica/comunidad/cdmx-adulto-muere-recibir-vacuna-anticovid',
            'publish_date'      => '2021-03-15',
            'source'            => 'Marzo 15, 2021 • Milenio',
            'content'           => 'Una persona de la tercera edad en la Ciudad de México murió tras recibir la primera dosis de la vacuna Pfizer contra covid-19, en la alcaldía Azcapotzalco, no obstante, las autoridades capitalinas descartaron que su deceso tenga relación con la aplicación de la dosis.',
        ]);

        //9 Abril 2021
        News::create([
            'news_category_id'  => $mexico->id,
            'title'             => 'Hay un protocolo para aplicar la vacuna: Ssa',
            'url'               => 'https://www.eluniversal.com.mx/nacion/hay-un-protocolo-para-aplicar-la-vacuna-ssa',
            'publish_date'      => '2021-04-07',
            'source'            => 'Abril 7, 2021 • El Universal',
            'content'           => 'Luego de que una enfermera simuló vacunar a un adulto mayor contra COVID-19, la Secretaría de Salud (Ssa) enfatizó que para cada dosis existe un protocolo que el personal debe seguir a la hora de aplicar la dosis.',
        ]);

        News::create([
            'news_category_id'  => $mexico->id,
            'title'             => 'Merck México, involucrado intensamente en la lucha contra el coronavirus',
            'url'               => 'http://www.pmfarma.com.mx/noticias/17981-merck-mexico-involucrado-intensamente-en-la-lucha-contra-el-coronavirus.html',
            'publish_date'      => '2021-04-07',
            'source'            => 'Abril 7, 2021 • PMFarma México',
            'content'           => 'Desde el inicio de la pandemia, Merck se ha involucrado intensamente en la lucha contra el coronavirus, al participar en más de 45 proyectos de vacunas en todo el mundo con productos y tecnologías. Dependiendo del proyecto, Merck apoya el proceso de investigación y desarrollo, para lo cual ofrece materias primas y productos como proteasas o anticuerpos.',
        ]);

        News::create([
            'news_category_id'  => $mexico->id,
            'title'             => 'Cofepris autoriza uso de emergencia de vacuna antiCOVID Covaxin, de la India',
            'url'               => 'https://www.milenio.com/politica/vacuna-covaxin-covid-19-india-aprobada-cofepris',
            'publish_date'      => '2021-04-06',
            'source'            => 'Abril 6, 2021 • Milenio',
            'content'           => 'Hugo López-Gatell, subsecretario de Salud, indicó que esta vacuna puede contribuir a ampliar el repertorio que se tiene en México; sin embargo, por el momento no hay un plan específico para adquirirla.',
        ]);

        News::create([
            'news_category_id'  => $mexico->id,
            'title'             => 'Ebrard viajará a Rusia, China, India y EU para gestionar dosis',
            'url'               => 'https://www.elfinanciero.com.mx/nacional/2021/04/06/ebrard-viajara-a-rusia-y-china-para-verificar-que-no-se-atrase-entrega-de-vacunas-covid-a-mexico/',
            'publish_date'      => '2021-04-06',
            'source'            => 'Abril 6, 2021 • El Financiero',
            'content'           => 'Marcelo Ebrard, secretario de Relaciones Exteriores (SRE), informó que viajará próximamente a Rusia y a China con el fin de verificar que los acuerdos de vacunas COVID estén en tiempo.',
        ]);

        News::create([
            'news_category_id'  => $mexico->id,
            'title'             => 'Suman 2 millones 251 mil 705 contagiados, refiere la Ssa',
            'url'               => 'https://www.eluniversal.com.mx/nacion/suman-2-millones-251-mil-705-contagiados-refiere-la-ssa',
            'publish_date'      => '2021-04-06',
            'source'            => 'Abril 6, 2021 • El Universal',
            'content'           => 'La Secretaría de Salud (Ssa) informó que México contabiliza 204 mil 339 defunciones por COVID-19, un incremento de 192 con respecto al día previo, así como mil 247 nuevos contagios del virus, para un acumulado de 2 millones 251 mil 705.',
        ]);

        News::create([
            'news_category_id'  => $mexico->id,
            'title'             => 'Birmex se declara en "terapia intensiva, intubada, muy mal"',
            'url'               => 'https://www.milenio.com/politica/birmex-se-declara-en-terapia-intensiva-intubada-muy-mal',
            'publish_date'      => '2021-04-06',
            'source'            => 'Abril 6, 2021 • Milenio',
            'content'           => 'Pese a que en el siglo pasado México era punta de lanza en el sector, en la actualidad no produce ninguna vacuna, y la empresa estatal dedicada al desarrollo de estos biológicos ahora solo se dedica a comprarlos y distribuirlos.',
        ]);

        News::create([
            'news_category_id'  => $mexico->id,
            'title'             => 'Falló la planeación gubernamental: Y la industria farmacéutica, en riesgo',
            'url'               => 'https://www.proceso.com.mx/reportajes/2021/4/6/fallo-la-planeacion-gubernamental-la-industria-farmaceutica-en-riesgo-261442.html',
            'publish_date'      => '2021-04-06',
            'source'            => 'Abril 6, 2021 • Revista Proceso',
            'content'           => 'La falta de una planeación gubernamental, dicen líderes del sector farmacéutico, los tiene paralizados y se corre el riesgo de un desabasto masivo de medicamentos y materiales de curación en todo el sector salud mexicano.',
        ]);

        News::create([
            'news_category_id'  => $mexico->id,
            'title'             => 'Estamos en las puertas de una nueva generación de vacunas',
            'url'               => 'https://www.elfinanciero.com.mx/salud/2021/04/06/estamos-en-las-puertas-de-una-nueva-generacion-de-vacunas/',
            'publish_date'      => '2021-04-06',
            'source'            => 'Abril 6, 2021 • El Financiero',
            'content'           => 'Es difícil creer que las vacunas alguna vez fueron consideradas un área estancada en la industria farmacéutica. Las vacunas casi siempre fueron difíciles de desarrollar, difíciles de fabricar y poco rentables. Afortunadamente, un puñado de fabricantes de medicamentos, startups e investigadores académicos continuaron buscando formas innovadoras para modernizar el proceso de desarrollo.',
        ]);

        News::create([
            'news_category_id'  => $mexico->id,
            'title'             => 'Con boteo, UAQ busca financiar su biológico',
            'url'               => 'https://www.razon.com.mx/mexico/boteo-uaq-busca-financiar-biologico-429723',
            'publish_date'      => '2021-04-06',
            'source'            => 'Abril 6, 2021 • La Razón',
            'content'           => 'Con boteo, servicio de mantenimiento a computadoras, eventos culturales y alcancías dentro de los campus, estudiantes y académicos trataron de reunir fondos para financiar las fases uno y dos de la vacuna COVID de la Universidad Autónoma de Querétaro (UAQ).',
        ]);

        News::create([
            'news_category_id'  => $mexico->id,
            'title'             => '¡Prueba no superada! Cofepris niega permiso a este fármaco contra la COVID-19',
            'url'               => 'https://www.saludiario.com/prueba-no-superada-cofepris-niega-permiso-a-este-farmaco-contra-la-covid-19/',
            'publish_date'      => '2021-04-04',
            'source'            => 'Abril 4, 2021 • Saludiario',
            'content'           => 'Después de una revisión el Comité de Moléculas Nuevas de la Cofepris dio una opinión no favorable al fármaco propuesto contra la COVID-19.',
        ]);

        News::create([
            'news_category_id'  => $mexico->id,
            'title'             => '¿Cómo está resolviendo México el suministro de medicamentos durante la pandemia?',
            'url'               => 'https://mexicobusiness.news/health/news/how-mexico-solving-supply-drugs-during-pandemic?tag=health&utm_source=ActiveCampaign&utm_medium=email&utm_content=Your+Weekly+Health+Briefing&utm_campaign=Newsletter-Health-31Mar',
            'publish_date'      => '2021-03-25',
            'source'            => 'Marzo 25, 2021 • Mexico Business News',
            'content'           => 'La atención a la crisis de salud en México se ha convertido en una situación crítica, tanto por el gran número de personas infectadas por el virus SARS-COV 2 que están en Terapia Intensiva y requieren intubación, como por la escasez de los medicamentos necesarios para realizar las intubaciones.',
        ]);



        // Latam
        News::create([
            'news_category_id'  => $latam->id,
            'title'             => 'Inmunidad de rebaño vs. COVID llegará a la mayoría de países de América Latina hasta 2023, afirma Cepal',
            'url'               => 'https://elfinanciero.com.mx/salud/inmunidad-de-rebano-vs-covid-llegara-a-la-mayoria-de-paises-de-america-latina-hasta-2023-afirma-cepal',
            'publish_date'      => '2021-03-16',
            'source'            => 'Marzo 16, 2021 • El Financiero',
            'content'           => '"La región no va a alcanzar la inmunidad de rebaño en 2021. En algunos países, 4 cuando mucho, (la alcanzarán) en 2021 o inicios de 2022. Otros más, alrededor de 7, en 2022 quizá. Y la mayoría la va alanzar hasta el 2023", alertó Alicia Bárcena, secretaria ejecutiva de la Comisión Económica para América Latina y el Caribe (Cepal).',
        ]);

        News::create([
            'news_category_id'  => $latam->id,
            'title'             => 'Brasil identifica una nueva variante de COVID-19 más contagiosa',
            'url'               => 'https://www.elfinanciero.com.mx/mundo/brasil-identifica-una-nueva-variante-de-covid-19-mas-contagiosa',
            'publish_date'      => '2021-03-12',
            'source'            => 'Marzo 12, 2021 • El Financiero',
            'content'           => 'Un grupo de investigadores de Brasil identificó una nueva mutación de COVID-19 que circula en diferentes regiones del país desde hace semanas y que, así como la llamada variante brasileña (P1), es más contagiosa que la original.',
        ]);

        News::create([
            'news_category_id'  => $latam->id,
            'title'             => 'Los sistemas digitales interconectados reducen el gasto y la desigualdad',
            'url'               => 'https://mexicobusiness.news/health/news/interconnected-digital-systems-reduce-expenditure-inequality?utm_source=ActiveCampaign&utm_medium=email&utm_content=Your+Weekly+Health+Briefing&utm_campaign=Newsletter+Health+10+marzo',
            'publish_date'      => '2021-03-03',
            'source'            => 'Marzo 3, 2021 • Mexico Business News',
            'content'           => 'Las ciudades digitales están transformando las vidas de las personas. ¿Cómo pueden impulsar el acceso a la salud en un país desigual?',
        ]);

        //9 Abril
        News::create([
            'news_category_id'  => $latam->id,
            'title'             => 'Coronavirus en Brasil: el país registra por primera vez más de 4.000 muertes en 24 horas',
            'url'               => 'https://www.bbc.com/mundo/noticias-america-latina-56658039',
            'publish_date'      => '2021-04-07',
            'source'            => 'Abril 7, 2021 • Noticias BBC',
            'content'           => 'Brasil se ha convertido en uno de los epicentros de la pandemia de coronavirus en el mundo. Por primera vez desde el inicio de la pandemia, el país sudamericano ha registrado más de 4.000 muertes relacionadas con el COVID-19 en un lapso de 24 horas, mientras el número de casos sigue aumentando impulsada por una variante más contagiosa del virus.',
        ]);

        News::create([
            'news_category_id'  => $latam->id,
            'title'             => 'América Latina y el Caribe superan las 800 mil muertes por COVID-19',
            'url'               => 'https://www.eluniversal.com.mx/mundo/america-latina-y-el-caribe-superaran-las-800-mil-muertes-por-covid-19',
            'publish_date'      => '2021-04-06',
            'source'            => 'Abril 6, 2021 • El Universal',
            'content'           => 'América Latina y el Caribe superaron este martes los 800 mil decesos por el COVID-19, mientras la vacunación avanza en el mundo de forma dispar y con dificultades, especialmente con el fármaco de AstraZeneca, del que se confirmó un vínculo con trombosis, según un responsable del regulador europeo.',
        ]);

        News::create([
            'news_category_id'  => $latam->id,
            'title'             => 'Chile decreta toque de queda pese al éxito de su plan de vacunación',
            'url'               => 'https://www.elfinanciero.com.mx/salud/2021/04/06/chile-decreta-toque-de-queda-pese-al-exito-de-su-plan-de-vacunacion/',
            'publish_date'      => '2021-04-06',
            'source'            => 'Abril 6, 2021 • El Universal',
            'content'           => 'Durante el fin de semana, el país registró más de 8 mil casos nuevos de contagio por dos días consecutivos. Se reportó un millón 32 mil 612 casos, 23 mil 677 muertes y 965 mil 641 recuperados.',
        ]);
        


        //Internacional
        News::create([
            'news_category_id'  => $internacional->id,
            'title'             => 'Da regulador de la UE apoyo a AstraZeneca',
            'url'               => 'https://www.heraldo.mx/da-regulador-de-la-ue-apoyo-a-astrazeneca/',
            'publish_date'      => '2021-03-17',
            'source'            => 'Marzo 17, 2021 • El Heraldo',
            'content'           => 'La Agencia Europea de Medicamentos instó a los Gobiernos a no detener el uso de la vacuna en un momento en que la pandemia todavía se cobra miles de vidas cada día.
            Existe una preocupación creciente de que incluso las suspensiones breves puedan tener efectos desastrosos en la confianza en las campañas de inmunización.',
        ]);

        News::create([
            'news_category_id'  => $internacional->id,
            'title'             => 'AstraZeneca: la OMS pide a países que sigan usando esta vacuna',
            'url'               => 'https://cnnespanol.cnn.com/video/astrazeneca-vacuna-suspension-oms-peticion-aplicacion-covd19-coagulos-sanguineos-europa-redaccion-mexico/',
            'publish_date'      => '2021-03-15',
            'source'            => 'Marzo 15, 2021 • CNN',
            'content'           => 'La Organización Mundial de la Salud (OMS) dijo que los países deberían seguir usando la vacuna de AstraZeneca contra el covid-19. Esto luego de que varios países europeos suspendieron su uso por casos de coágulos sanguíneos en algunos individuos. La OMS indica que no necesariamente hay un vínculo, pero informó que habrá una reunión para analizar la seguridad de las vacunas este martes.',
        ]);

        News::create([
            'news_category_id'  => $internacional->id,
            'title'             => 'Italia se encierra de nuevo por la virulencia de las cepas',
            'url'               => 'https://www.larazon.es/internacional/20210315/37evepnxlnfr3lwztbjm4hiume.html',
            'publish_date'      => '2021-03-15',
            'source'            => 'Marzo 15, 2021 • La Razón',
            'content'           => 'Cerca de 42 millones de italianos tienen prohibido desde hoy salir de casa si no es para ir a trabajar. No se permite abandonar el propio municipio, visitar a parientes o amigos, acudir a segundos domicilios o ir a cualquier negocio que no sea considerado esencial, porque están cerrados. Con lo cual, quedan pocas vías de escape más que salir a un parque o pasear por la calle.',
        ]);

        News::create([
            'news_category_id'  => $internacional->id,
            'title'             => 'Hace un año se declaró la pandemia de COVID-19, ¿qué ha hecho la OMS en este tiempo?',
            'url'               => 'https://elfinanciero.com.mx/salud/hace-un-ano-se-declaro-la-pandemia-de-covid-19-que-ha-hecho-la-oms-en-este-tiempo',
            'publish_date'      => '2021-03-11',
            'source'            => 'Marzo 11, 2021 • El Financiero',
            'content'           => 'Cuando declaró la pandemia del coronavirus hace un año, la Organización Mundial de la Salud (OMS) actuó semanas después de resistirse a usar ese término y asegurando que todavía era posible contener el virus. Un año más tarde, el organismo de las Naciones Unidas sigue luchando por controlar los contagios mediante la ciencia, por persuadir a los países de que abandonen sus tendencias nacionalistas y por hacer llegar las vacunas adonde son más necesitadas.',
        ]);

        News::create([
            'news_category_id'  => $internacional->id,
            'title'             => '¡Buenas noticias! Vacuna COVID de Novavax tiene efectividad de 86% contra variante de Reino Unido',
            'url'               => 'https://elfinanciero.com.mx/salud/buenas-noticias-vacuna-covid-de-novavax-tiene-efectividad-de-86-contra-variante-de-reino-unido',
            'publish_date'      => '2021-03-11',
            'source'            => 'Marzo 11, 2021 • El Financiero',
            'content'           => 'Una nueva vacuna contra el COVID-19 se acerca a la línea de meta, después de que Novavax informó este jueves que su producto evitó hospitalizaciones y fallecimientos durante los ensayos realizados en Reino Unido y Sudáfrica, donde circulan mutaciones del coronavirus.',
        ]);

        //9 Abril
        News::create([
            'news_category_id'  => $internacional->id,
            'title'             => 'Fondo ruso acuerda con ocho fabricantes de tres naciones producir mil 100 millones de biológicos',
            'url'               => 'https://www.jornada.com.mx/notas/2021/04/06/politica/fondo-ruso-acuerda-con-ocho-fabricantes-de-tres-naciones-producir-mil-100-millones-de-biologicos/',
            'publish_date'      => '2021-04-06',
            'source'            => 'Abril 6, 2021 • La Jornada',
            'content'           => 'El Fondo de Inversión Directa de Rusia concretó acuerdos con ocho fabricantes de India, China y Corea para la producción de más de mil 100 millones de vacunas Sputnik V, cuyo uso de emergencia está aprobado en 59 países, incluido México.',
        ]);

        News::create([
            'news_category_id'  => $internacional->id,
            'title'             => 'Alarma por cifras récord de contagios en el mundo',
            'url'               => 'https://www.jornada.com.mx/2021/04/06/politica/007n1pol',
            'publish_date'      => '2021-04-06',
            'source'            => 'Abril 6, 2021 • La Jornada',
            'content'           => 'Nueva Delhi., India, Venezuela, Uruguay, Irán, China, España y Austria anunciaron ayer récords de contagios, que a nivel global han provocado más de 2 millones 858 mil muertes y supera 131 millones 666 mil casos.',
        ]);

        News::create([
            'news_category_id'  => $internacional->id,
            'title'             => 'China llama a eliminar "brecha inmunológica"',
            'url'               => 'https://www.razon.com.mx/mundo/china-llama-eliminar-brecha-inmunologica-429711',
            'publish_date'      => '2021-04-06',
            'source'            => 'Abril 6, 2021 • La Jornada',
            'content'           => 'China convocó a actuar contra el nacionalismo en la lucha contra el COVID-19 y eliminar la "brecha inmunológica", luego de denunciar que algunas naciones ricas han adquirido hasta el triple de vacunas de las que necesita.',
        ]);

        News::create([
            'news_category_id'  => $internacional->id,
            'title'             => '¡Descubren cual es la causa de los trombos en la vacuna de AstraZeneca!',
            'url'               => 'https://www.saludiario.com/descubren-cual-es-la-causa-de-los-trombos-en-la-vacuna-de-astrazeneca/',
            'publish_date'      => '2021-04-06',
            'source'            => 'Abril 6, 2021 • Saludiario',
            'content'           => 'Según el estudio alemán publicado en Research Square; Un trastorno trombocitopénico protrombótico que se asemeja a la trombocitopenia inducida por heparina después de la vacunación contra el COVID-19. Se sugiere que estos eventos se asemejan a un trastorno conocido como; la trombocitopenia inducida por heparina (TIH), que se puede tratar si se identifica de inmediato.',
        ]);


        //Esados Unidos
        News::create([
            'news_category_id'  => $usa->id,
            'title'             => 'Detectan 2 nuevas variantes de coronavirus en EE.UU.',
            'url'               => 'https://cnnespanol.cnn.com/video/nuevas-cepas-variantes-coronavirus-covid-19-california-eeuu-vo-alejandra-oraa-cafe-cnn/',
            'publish_date'      => '2021-03-17',
            'source'            => 'Marzo 17, 2021 • Noticias CNN',
            'content'           => 'En Estados Unidos, las autoridades están preocupadas por 2 nuevas cepas de coronavirus detectadas en California. Según los Centros para el Control y la Prevención de Enfermedades de Estados Unidos, CDC por sus siglas en inglés, estas variantes pueden ser aproximadamente un 20% más transmisibles y algunos tratamientos contra el covid-19 pueden ser menos efectivos contra ellas.',
        ]);

        News::create([
            'news_category_id'  => $usa->id,
            'title'             => 'Coronavirus: el caso del hombre con superanticuerpos contra la COVID-19 (y por qué da esperanza a los científicos)',
            'url'               => 'https://www.bbc.com/mundo/noticias-56430552',
            'publish_date'      => '2021-03-17',
            'source'            => 'Marzo 17, 2021 • Noticias BBC',
            'content'           => 'En la mayoría de las personas, los anticuerpos que se generan para combatir el virus atacan las proteínas de las espículas del coronavirus, formaciones puntiagudas en la superficie del Sars-Cov-2 que le ayudan a infectar las células humanas. Pero los anticuerpos de Hollis son distintos: atacan varias partes del virus y lo eliminan rápidamente.',
        ]);

        News::create([
            'news_category_id'  => $usa->id,
            'title'             => 'Moderna prueba ya su vacuna en menores de 12 años',
            'url'               => 'https://www.abc.es/internacional/abci-moderna-prueba-vacuna-menores-12-anos-202103162057_video.html?ref=https:%2F%2Fwww.google.com%2F',
            'publish_date'      => '2021-03-16',
            'source'            => 'Marzo 16, 2021 • Noticias ABC',
            'content'           => 'Los viales de la vacuna de Moderna empiezan a administrarse a niños de entre 6 meses y menos de 12 años. Más de 6.700 menores estadounidenses y canadienses participan en un ensayo clínico que evaluará la seguridad y eficacia de sus dos dosis en esa franja de edad.',
        ]);

        //9 Abril
        News::create([
            'news_category_id'  => $usa->id,
            'title'             => 'Abbott anuncia su Coalición de Defensa Contra Pandemias, una red global de expertos dedicados a prevenir futuras pandemias',
            'url'               => 'http://www.pmfarma.com.mx/noticias/17978-abbott-anuncia-su-coalicion-de-defensa-contra-pandemias-una-red-global-de-expertos-dedicados-a-prevenir-futuras-pandemias.html',
            'publish_date'      => '2021-04-07',
            'source'            => 'Abril 7, 2021 • PMFarma México',
            'content'           => 'La Coalición de Defensa contra Pandemias de Abbott es la primera red científica mundial dedicada a la detección temprana y respuesta rápida a futuras amenazas pandémicas. La coalición está construida sobre décadas de liderazgo de Abbott en vigilancia de virus y ayuda a analizar muestras de virus para detectar enfermedades desconocidas, mutaciones y variantes, incluidas las de COVID-19.',
        ]);

        News::create([
            'news_category_id'  => $usa->id,
            'title'             => 'Coronavirus en EE.UU.: 3 claves que explican el vertiginoso ritmo de vacunación contra la COVID-19 en el país más golpeado por la pandemia',
            'url'               => 'https://www.bbc.com/mundo/noticias-internacional-56657248',
            'publish_date'      => '2021-04-07',
            'source'            => 'Abril 7, 2021 • Noticias BBC',
            'content'           => 'Estados Unidos está superando sus previsiones más optimistas de vacunación. Este martes, el gobierno anunció que a partir del 19 de abril los adultos de cualquier edad podrán recibir la vacuna, mucho antes de lo previsto por el presidente Joe Biden, quien desde que asumió la presidencia el 20 de enero hizo de la vacunación su prioridad.',
        ]);
       
    }
}
