<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->name             = 'Rafael Lemus G.';
        $user->email            = 'rafael@playproducciones.com.mx';
        $user->password         =  Hash::make('secret');
        $user->save();
        $user->assignRole('admin');
    }
}
