<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RoleSeeder::class,
            UserSeeder::class,
            CourseSeeder::class,
            WebinarSeeder::class,
            NewSeeder::class,
            PaperSeeder::class,
            ScaleHamiltonSeeder::class,
            ScaleBeckAnxietySeeder::class,
            ScaleBeckDepressionSeeder::class,
            ScaleFolsteinSeeder::class,
            ScaleGDSSeeder::class
        ]);
    }
}
