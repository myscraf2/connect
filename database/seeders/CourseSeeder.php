<?php

namespace Database\Seeders;

use App\Models\Course;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Schema::disableForeignKeyConstraints();
            DB::table('courses')->truncate();
        Schema::enableForeignKeyConstraints();

        Course::create([
            'title'         => '<b>Influenza:</b> Historia de la <b>Psiquiatría de las Pandemias</b>, Hechos Históricos y Hombres de Ciencia que las Vencieron',
            'speaker'       => 'Dra. Irma Corlay Noriega - México',
            'poster_url'    => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/posters/Influenza%20Dra.%20Corlay.jpg',
            'video_url'     => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/videos/Influenza_Dra%20Irma%20Corlay%20Noriega_10DIC20aa.mp4',
            'publish_date'  => '2020-12-10',
            'tags'          => ['pandemia', 'influenza', 'historia','psiquiatría'],
        ]);

        Course::create([
            'title'         => '<b>Primer Brote Psicótico</b>, del Laboratorio al Consultorio',
            'speaker'       => 'Dr. Bernardo Ng Solís  (México) -  Dr. Juan Gallego (E.U.A.)',
            'poster_url'    => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/posters/PRIMER%20BROTE%20PSICOTICO%20DR%20BERNARDO%20NG.jpg',
            'video_url'     => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/videos/Encuentro%20Internacional%20de%20Expertos%20Apopharma%20Primer%20Episodio%20Psico%CC%81tico,%20del%20Laboratorio%20al%20Consultorio-20201203%200102-1.mp4',
            'publish_date'  => '2020-12-02',
            'tags'          => ['primer', 'brote', 'psicotico'],
        ]);

        Course::create([
            'title'         => 'Enfermedad de <b>Alzheimer, Nuevas Estrategias Terapéuticas</b>: ¿Dónde Estamos y Hacia Dónde Vamos?',
            'speaker'       => 'Dr. Janus Kremer - Argentina',
            'poster_url'    => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/posters/ALZHEIMER%20DR%20JANUS%20KREMER.jpg',
            'video_url'     => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/videos/Alzheimer%20en%20Tiempos%20de%20COVID-19,%20Diagno%CC%81stico%20y%20Tratamientos%20Actuales-20201119%202202-1.mp4',
            'publish_date'  => '2020-11-19',
            'tags'          => ['alzheimer', 'estrategias', 'terapéuticas'],
        ]);

        Course::create([
            'title'         => '<b>Tuberculosis</b>: Historia de la <b>Psiquiatría de las Pandemias</b>, Hechos Históricos y Hombres de Ciencia que las Vencieron',
            'speaker'       => 'Dra. Irma Corlay Noriega - México',
            'poster_url'    => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/posters/HISTORIA%20PSIQUIATRIA%20TUBERCULOSIS.jpg',
            'video_url'     => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/videos/Tuberculosis_Dra%20Irma%20Corlay%20Noriega_12NOV20_001.mp4',
            'publish_date'  => '2020-11-19',
            'tags'          => ['pandemia', 'tuberculosis', 'historia', 'psiquiatría'],
        ]);

        Course::create([
            'title'         => '<b>Avances</b> Tecnológicos en <b>Diagnóstico</b> y <b>Manejo Neuropsiquiátrico</b>',
            'speaker'       => 'Dr. Pedro M. Sánchez Gómez - España',
            'poster_url'    => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/posters/DR%20PEDRO%20SANCHEZ%20AVANCES%20TECONOLOGICOS.jpg',
            'video_url'     => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/videos/Avances%20Tecnolo%CC%81gicos%20en%20Diagno%CC%81stico%20y%20Manejo%20Neuropsiquia%CC%81trico-20201028%201704-1.mp4',
            'publish_date'  => '2020-10-28',
            'tags'          => ['avances', 'diagnóstico', 'neuropsiquiátrico'],
        ]);

        Course::create([
            'title'         => '<b>Sífilis</b>: Historia de la <b>Psiquiatría de las Pandemias</b>, Hechos Históricos y Hombres de Ciencia que las Vencieron',
            'speaker'       => 'Dra. Irma Corlay Noriega - México',
            'poster_url'    => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/posters/HISTORIA%20PSIQUIATRIA%20SIFILIS.jpg',
            'video_url'     => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/videos/Talleres%20de%20Vanguardia_Historia%20de%20las%20Epidemias_Sifilis_08OCT20_c2a.mp4',
            'publish_date'  => '2020-10-08',
            'tags'          => ['pandemia', 'sífilis', 'historia', 'psiquiatría'],
        ]);

        Course::create([
            'title'         => 'Actualidades y Retos en el <b>Abordaje del Dolor Neuropático</b>: Dimensiones Clínicas y Manejo Integral',
            'speaker'       => 'Dr. Erwin Chiquete - México',
            'poster_url'    => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/Dolor%20Neuropatico.jpg',
            'video_url'     => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/videos/Webinar%20Actualidades%20y%20Retos%20en%20el%20Abordaje%20del%20Dolor%20Neurop%C3%A1tico%20Dimensiones%20Cl%C3%ADnicas%20y%20Manejo%20Integral-20200910%202104-1.mp4',
            'publish_date'  => '2020-09-10',
            'tags'          => ['dolor', 'neuropático','retos'],
        ]);

        Course::create([
            'title'         => '<b>Malaria</b>: Historia de la <b>Psiquiatría de las Pandemias</b>, Hechos Históricos y Hombres de Ciencia que las Vencieron',
            'speaker'       => 'Dra. Irma Corlay Noriega - México',
            'poster_url'    => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/Malaria.jpg',
            'video_url'     => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/videos/Talleres%20de%20Vanguardia_Historia%20de%20las%20Epidemias_Malaria_03SEP20_c1.mp4',
            'publish_date'  => '2020-09-3',
            'tags'          => ['pandemia', 'malaria', 'historia'],
        ]);

        Course::create([
            'title'         => 'Consideraciones Etiológicas de los <b>Trastornos Neurocognitivos en México</b> Perspectivas de Prevención',
            'speaker'       => 'Dr. Bernardo Ng Solis - México',
            'poster_url'    => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/Trastornos%20Neurocognitivos.jpg',
            'video_url'     => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/videos/Webinar%20Consideraciones%20Etiol%C3%B3gicas%20de%20los%20Trastornos%20Neurocognitivos%20en%20M%C3%A9xico%20Perspectivas%20de%20Prevenci%C3%B3n%20y%20T-20200820%200103-1.mp4',
            'publish_date'  => '2020-08-19',
            'tags'          => ['etiológicas', 'trastornos', 'neurocognitivos','prevención'],
        ]);

        Course::create([
            'title'         => '<b>Multimorbilidad y Fragilidad</b>: Abordar la Complejidad en la <b>Enfermedad de Parkinson</b> y sus Retos',
            'speaker'       => 'Dr. Mayela Rodríguez Violante - México',
            'poster_url'    => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/Parkinson.jpg',
            'video_url'     => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/videos/Webinar%20%E2%80%9CMultimorbilidad%20y%20Fragilidad%20Abordar%20la%20Complejidad%20en%20la%20Enfermedad%20de%20Parkinson%20y%20sus%20Retos%E2%80%9D-20200813%202107-1.mp4',
            'publish_date'  => '2020-08-13',
            'tags'          => ['complejidad', 'parkinson', 'retos'],
        ]);

        Course::create([
            'title'         => 'Como combinar las técnicas de <b>neuromodulación</b> en el manejo de la <b>depresión, dolor crónico</b> y <b>trastornos del movimiento</b>',
            'speaker'       => 'Dr. Edilberto Peña de León - México',
            'poster_url'    => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/Neuromodulacio%CC%81n.jpg',
            'video_url'     => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/videos/Webinar%20Como%20Combinar%20las%20T%C3%A9cnicas%20de%20Neuromodulaci%C3%B3n%20en%20el%20Manejo%20de%20la%20Depresi%C3%B3n,%20Dolor%20Cr%C3%B3nico%20y%20Trastornos-20200807%200105-1.mp4',
            'publish_date'  => '2020-08-06',
            'tags'          => ['tecnicas', 'neuromodulación', 'depresión','dolor'],
        ]);

        Course::create([
            'title'         => '<b>Viruela</b>: Historia de la <b>Psiquiatría de las Pandemias</b>, Hechos Históricos y Hombres de Ciencia que las Vencieron',
            'speaker'       => 'Dra. Irma Corlay Noriega - México',
            'poster_url'    => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/Viruela.jpg',
            'video_url'     => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/videos/Talleres%20Apopharma_20JUL20_Historia%20de%20las%20Pandemias%20Editado.mp4',
            'publish_date'  => '2020-07-30',
            'tags'          => ['pandemia', 'viruela', 'historia'],
        ]);


        Course::create([
            'title'         => '<b>Trastornos Neuropsiquiátricos</b> en la Pandemia del <b>COVID-19</b>',
            'speaker'       => 'Dr. Edilberto Peña de León - México',
            'poster_url'    => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/posters/Trastornos%20Neuropsiquiatricos%20COVID%2019%20DR%20EDILBERTO%20PEN%CC%83A.jpg',
            'video_url'     => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/videos/Webinar%20Trastornos%20Neuropsiquia%CC%81tricos%20por%20COVID-19-20200626%200125-1.mp4',
            'publish_date'  => '2020-07-15',
            'tags'          => ['trastornos', 'neuropsiquiátricos', 'covid-19', 'pandemia'],
        ]);

        //FIA
        Course::create([
            'title'         => 'Futuro en el Tratamiento de las <b>Demencias</b>',
            'speaker'       => 'Dr. Adrián Martínez Ruiz | Nueva Zelanda',
            'poster_url'    => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/posters/FIA.png',
            'video_url'     => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/videos/FIA%20-%20APOTEX%20-%20FEBRUARY%2018%20VF.mp4',
            'publish_date'  => '2021-02-18',
        ]);

        Course::create([
            'title'         => 'Protocolos de <b>Detección Temprana</b> y Tratamiento Oportuno en el <b>TDAH en Niños</b>',
            'speaker'       => 'Dra. Jisbelys Salazar | Venezuela',
            'poster_url'    => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/posters/FIA.png',
            'video_url'     => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/videos/FIA%20-%20APOTEX%20-%20FEBRUARY%2017%20VF.mp4',
            'publish_date'  => '2021-02-17',
        ]);

        Course::create([
            'title'         => 'Algoritmos de <b>Depresión Resistente</b> a Tratamiento',
            'speaker'       => 'Dr. Marcelo Cetkovich | Argentina',
            'poster_url'    => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/posters/FIA.png',
            'video_url'     => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/videos/FIA%20-%20APOTEX%20-%20FEBRUARY%2016%20VF.mp4',
            'publish_date'  => '2021-02-16',
        ]);

        Course::create([
            'title'         => '<b>Primer Episodio Psicótico:</b>  Estudio en Población Latinoamericana',
            'speaker'       => 'Dr. Rodrigo Córdoba | Colombia',
            'poster_url'    => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/posters/FIA.png',
            'video_url'     => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/videos/FIA%20-%20APOTEX%20-%20FEBRUARY%2011%20VF.mp4',
            'publish_date'  => '2021-02-11',
        ]);

        Course::create([
            'title'         => 'Técnicas de Neuromodulación en <b>Trastornos del Movimiento</b>',
            'speaker'       => 'Dr. Adolfo Ramírez Zamora | E.U.A.',
            'poster_url'    => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/posters/FIA.png',
            'video_url'     => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/videos/FIA%20-%20APOTEX%20-%20FEBRUARY%2010%20VF.mp4',
            'publish_date'  => '2021-02-10',
        ]);

        Course::create([
            'title'         => 'Avances Tecnológicos en <b>Inflamación Neuropsiquiátrica</b>',
            'speaker'       => 'Dr. Adolfo Ramírez Zamora | E.U.A.',
            'poster_url'    => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/posters/FIA.png',
            'video_url'     => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/videos/FIA%20-%20APOTEX%20-%20FEBRUARY%2009%20VF.mp4',
            'publish_date'  => '2021-02-09',
        ]);

        Course::create([
            'title'         => 'Cómo obtener el <b>mayor provecho</b> de las <b>herramientas digitales</b> en la <b>consulta</b> diaria',
            'speaker'       => 'Maestro Enrique Culebro Karam (México) - QFB Jose Iván Reséndiz (México) - Dr. Edilberto Peña de León (México)',
            'poster_url'    => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/posters/APONET.png',
            'video_url'     => 'https://aconnect.sfo2.digitaloceanspaces.com/courses/videos/VIDEO%20LANZAMIENTO%20APONET%208ABR21.mp4',
            'publish_date'  => '2021-04-08',
        ]);
        
    }
}
