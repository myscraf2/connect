const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    purge: [
        './vendor/laravel/jetstream/**/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
        './resources/js/**/*.vue',
    ],

    theme: {
        extend: {
            fontFamily: {
                sans: ['Nunito', ...defaultTheme.fontFamily.sans],
                exo2: ['"Exo 2"'],
            },
            colors: {
                'blue-app':  {                    
                    100: '#00a2da',
                    200: '#2e70b5',
                    300: '#00d4fc',
                    400: '#53a8dc',
                    500: '#0086cf',
                    600: '#006dc3',
                    700: '#004883'
                },
                'green-app': '#02a89c'
            },
            height: {
                100: '22rem'
            }
        },
    },

    variants: {
        opacity: ['responsive', 'hover', 'focus', 'disabled'],
    },

    plugins: [require('@tailwindcss/ui')],
};
