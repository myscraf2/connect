<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreScale extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required',
            'age'       => 'required',
            'date'      => 'required|date',
            'answers'   => 'required|array',
            'answers.*' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'answers.*.required' => 'Falta contestar esta pregunta'
        ];
    }
}
