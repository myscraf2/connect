<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

class EnsureLicenseIsVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (! $request->user() || (! $request->user()->hasVerifiedLicense())) {
            return $request->expectsJson()
                    ? abort(403, 'Your professional license is not verified.')
                    : Redirect::guest(URL::route('license.notice'));
        }
        return $next($request);
    }
}
