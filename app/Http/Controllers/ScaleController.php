<?php

namespace App\Http\Controllers;

use App\Enums\ScaleType;
use App\Http\Requests\StoreScale;
use App\Models\Scale;
use App\Models\ScaleResult;
use App\Models\ScaleResultDetail;
use App\Notifications\ScaleSend;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Inertia\Inertia;
use PDF;

class ScaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inertia::render('Scales');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreScale $request)
    {
        return DB::transaction(function () use ($request) {

            $scaleResult = new ScaleResult;
            $scaleResult->fill($request->validated());
            $scaleResult->user_id = auth()->user()->id;
            $scaleResult->scale_id = $request->scale['id'];
            $scaleResult->save();

            foreach ($request->answers as $answer) {
                ScaleResultDetail::create([
                    'scale_result_id'   => $scaleResult->id,
                    'scale_question_id' => $answer['scale_question_id'],
                    'scale_answer_id'   => $answer['scale_answer_id'],
                    'value'             => $answer['value'],
                ]);
            }

            return response()->json([
                'result_id' => $scaleResult->id,
                'score'     => $scaleResult->getScore(),
                'diagnosis' => $scaleResult->getDiagnosis()
            ]);
        });
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $scales = Scale::with('questions', 'questions.answers')
            ->where('type', ScaleType::getValue($id))->get();
        $name = ucfirst(trans($id));
        return Inertia::render('Scales/Show', compact('scales','name'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function pdf($id)
    {
        $result = ScaleResult::find($id);
        $pdf = PDF::loadView('pdfs.scale', compact('result'));
        return $pdf->download($result->getPdfFileName());
    }

    public function email($id)
    {
        $result = ScaleResult::find($id);
        $pdf = PDF::loadView('pdfs.scale', compact('result'));
        $output = $pdf->output();

        $result->user->notify(new ScaleSend($result, $output));

        return response()->json([
            'message' => 'Escala enviada al correo ' . auth()->user()->email
        ]);
    }
}
