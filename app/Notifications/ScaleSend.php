<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ScaleSend extends Notification
{
    use Queueable;

    private $result, $output;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($result, $output)
    {
        $this->result = $result;
        $this->output = $output;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $subject = 'Resultado Escala ' . $this->result->scale->title . ' ' . $this->result->name;
        return (new MailMessage)
            ->subject($subject)
            ->attachData($this->output, $this->result->getPdfFileName(), [
                'mime' => 'application/pdf',
            ])
            ->markdown('mail.scale.send', ['result' => $this->result]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
