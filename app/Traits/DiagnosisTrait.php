<?php

namespace App\Traits;

trait DiagnosisTrait
{
    public function getScaleDiagnosis($model)
    {        
        return $this->{$model->scale->name}($model);        
    }

    private function hamilton($model)
    {
        $score = $model->getScore();
        if ($score <= 5) {
            return "<b>Ausencia o Remisión del Trastorno</b>";
        } else if ($score > 5 && $score <= 14) {
            return "Ansiedad <b>Leve</b>";
        } else if ($score >= 15) {
            return "Ansiedad <b>Moderada/Grave</b> (Amerita Tratamiento)";
        }
    }

    private function beck_anxiety($model)
    {
        $score = $model->getScore();
        if ($score <= 5) {
            return "<b>Ansiedad Mínima</b>";
        } else if ($score > 5 && $score <= 15) {
            return "<b>Ansiedad Leve</b>";
        } else if ($score > 15 && $score <= 30) {
            return "<b>Ansiedad Moderada</b>";
        } else if ($score >= 31) {
            return "<b>Ansiedad Severa</b>";
        }
    }

    private function beck_depression($model)
    {
        $score = $model->getScore();
        if ($score <= 10) {
            return "Estos Altibajos son Considerados Normales.";
        } else if ($score >= 11 && $score <= 16) {
            return "Leve Perturbación del Estado de Ánimo.";
        } else if ($score >= 17 && $score <= 20) {
            return "Estados de Depresión Intermitentes.";
        } else if ($score >= 21 && $score <= 30) {
            return "Depresión <b>Moderada</b>.";
        } else if ($score >= 31 && $score <= 40) {
            return "Depresión <b>Grave</b>.";
        } else if ($score > 40) {
            return "Depresión <b>Extrema</b>.";
        }
    }

    private function folstein($model)
    {
        $score = $model->getScore();
        if ($score < 14) {
            return "Grado de Deterioro Cognoscitivo<br><b>Grave</b>";
        } else if ($score >= 14 && $score <= 18) {
            return "Grado de Deterioro Cognoscitivo<br><b>Moderado</b>";
        } else if ($score >= 19 && $score <= 23) {
            return "Grado de Deterioro Cognoscitivo<br><b>Leve</b>";
        } else if ($score > 23) {
            return "Grado de Deterioro Cognoscitivo<br><b>Normal</b>";
        }
    }

    private function gds($model)
    {
        $score = $model->getScore();
        if ($score < 6) {
            return "Normal";
        } else if ($score >= 6 && $score <= 9) {
            return "Depresión <b>Leve</b>";
        } else if ($score > 9) {
            return "Depresión <b>Establecida</b>";
        }
    }
}
