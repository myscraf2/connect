<?php

namespace App\Console\Commands;

use App\Imports\NewsImport;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

class PlayImports extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'play:imports';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import from excel layouts info';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info("############# Importando Noticias #######################");
        Excel::import(new NewsImport, 'imports/news_layout.xlsx');
    }
}
