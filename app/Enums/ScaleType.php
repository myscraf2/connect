<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class ScaleType extends Enum
{
    const anxiety       = 1;
    const depression    = 2;
    const alzheimer     = 3;

    public static function getDescription($value): string
    {
        return mb_strtolower(parent::getDescription($value));
    }
}
