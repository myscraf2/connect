<?php

namespace App\Imports;

use App\Models\News;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class NewsImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new News([
            'news_category_id'  => $row['news_category_id'],
            'title'             => $row['title'],
            'url'               => $row['url'],
            'publish_date'      => Date::excelToDateTimeObject($row['publish_date'])->format('Y-m-d'),
            'source'            => $row['source'],
            'content'           => $row['content'],
        ]);
    }
}
