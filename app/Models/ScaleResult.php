<?php

namespace App\Models;

use App\Traits\DiagnosisTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ScaleResult extends Model
{
    use HasFactory, DiagnosisTrait;

    protected $fillable = [
        'name',
        'age',
        'date',
    ];


    public function scale()
    {
        return $this->belongsTo(Scale::class);
    }

    public function details()
    {
        return $this->hasMany(ScaleResultDetail::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    //Mutators
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = mb_strtoupper($value);
    }

    //Methods
    public function getScore()
    {
        return $this->details()->sum('value');
    }

    public function getDiagnosis()
    {
        return $this->getScaleDiagnosis($this);
    }

    public function getPdfFileName()
    {
        $fecha = \Carbon\Carbon::parse($this->date)->locale('es');
        return 'Escala ' . $this->scale->title . ' ' . $fecha->format('d') . ' ' . mb_strtoupper($fecha->shortMonthName) . ' ' . $fecha->format('y') . '.pdf';
    }
}
