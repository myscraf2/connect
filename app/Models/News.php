<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use HasFactory;

    protected $fillable = [
        'news_category_id',
        'title',
        'url',
        'publish_date',
        'source',
        'content'
    ];

    public function news_category()
    {
        return $this->belongsTo(NewsCategory::class);
    }
}
