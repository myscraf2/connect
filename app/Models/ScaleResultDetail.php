<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ScaleResultDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'scale_result_id',
        'scale_question_id',
        'scale_answer_id',
        'value',
    ];

    public function question()
    {
        return $this->belongsTo(ScaleQuestion::class,'scale_question_id');
    }

    public function answer()
    {
        return $this->belongsTo(ScaleAnswer::class,'scale_answer_id');
    }
}
